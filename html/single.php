<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>

	<link rel="stylesheet" href="style.css">
</head>
<body>

	<div class="wrapper">
		<header class="header">
			<div class="container">
				<div class="branding"><a href="" title=""><img src="assets/images/branding.png" height="64" width="178" alt=""></a></div>

				<nav class="nav navigation-header">
					<ul id="menu-menu-principal" class="menu"><li id="menu-item-5" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-5"><a href="http://agrosb.apikiprojeto.com/">Home</a></li>
						<li id="menu-item-429" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-429"><a href="http://agrosb.apikiprojeto.com/home/a-agro-sb/">A Empresa</a>
						<li id="menu-item-263" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-263"><a href="http://agrosb.apikiprojeto.com/noticias/">Notícias</a></li>
						<li id="menu-item-239" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-239"><a href="http://agrosb.apikiprojeto.com/comercial/">Comercial</a>
						<ul class="sub-menu">
							<li id="menu-item-548" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-548"><a href="http://agrosb.apikiprojeto.com/leiloes-e-feiras/">Leilões e Feiras</a></li>
							<li id="menu-item-621" class="menu-item menu-item-type-taxonomy menu-item-object-agro_assets_for_sale menu-item-621"><a href="http://agrosb.apikiprojeto.com/bens-a-venda/venda-especiais/">Venda Direta</a></li>
							<li id="menu-item-978" class="menu-item menu-item-type-taxonomy menu-item-object-agro_assets_for_sale menu-item-978"><a href="http://agrosb.apikiprojeto.com/bens-a-venda/outros-produtos/">Outros Produtos</a></li>
							<li id="menu-item-612" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-612"><a href="http://www.bradesco.com.br/html/classic/produtos-servicos/outros/2-via-de-boleto.shtm">2ª via de boleto</a></li>
						</ul>
						</li>
						<li id="menu-item-550" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-550"><a href="http://agrosb.apikiprojeto.com/fale-conosco/">Fale Conosco</a></li>
					</ul>
				</nav>
				
				<div class="quick-search">
					<form action="">
						<label for="">Busca Rápida</label>
						<input type="text" placeholder="Código do Imóvel">
						<input type="submit" value="ok">
					</form>					
				</div>
			</div>
		</header><!-- header -->

		<section class="entry-title">
			<div class="container">
				<h1 class="title">Apartamento - CÓD:IP 738</h1>
			</div>
		</section>

		<section class="container">
			<section class="content single-property">
				<header class="header-price">
					<span class="price">Valor: R$ 2000</span>
				</header>

				<div class="slider">
					<div class="wrap-media" data-component-gallery>
						<div class="media-slide-featured">
					    	<figure data-attr-figure class="media-slide-figure">
					    		<a href="javascript:void(0);" title="title">
					    			<img class="first" src="http://placehold.it/510x345" alt="thumbnail-large">
						    		<figcaption class="caption">Lorem ipsum dolor.</figcaption>
					    		</a>
					    	</figure>

					    	<div class="media-slide-counter">
								Imagem <span data-attr-counter>1</span> de 3
							</div>
						</div><!-- media-slide-featured -->

					    <div class="media-slide" data-attr-thumbs>
							<ul>
					    		<li>
					    			<a href="javascript:void(0);" data-attr-index="" data-attr-image='' title="uque">
						    			<img src="http://placehold.it/155x105" alt="thumbnail">
						    		</a>
					    		</li>
					    		<li>
					    			<a href="javascript:void(0);" data-attr-index="" data-attr-image='' title="uque">
						    			<img src="http://placehold.it/155x105" alt="thumbnail">
						    		</a>
					    		</li>
					    		<li>
					    			<a href="javascript:void(0);" data-attr-index="" data-attr-image='' title="uque">
						    			<img src="http://placehold.it/155x105" alt="thumbnail">
						    		</a>
					    		</li>
					    		<li>
					    			<a href="javascript:void(0);" data-attr-index="" data-attr-image='' title="uque">
						    			<img src="http://placehold.it/155x105" alt="thumbnail">
						    		</a>
					    		</li>					    							    		
							</ul>
					    </div><!-- media-slide -->
				    </div><!-- wrap-media -->					
				</div>

				<div class="details">
					<h3 class="title large">Detalhes do Imóvel</h3>

					<ul class="list">
						<li><strong>Amenities:</strong> Lorem ipsum.</li>
						<li><strong>Amenities:</strong> Lorem ipsum.</li>
						<li><strong>Amenities:</strong> Lorem ipsum.</li>
						<li><strong>Amenities:</strong> Lorem ipsum dolor..</li>
						<li><strong>Amenities:</strong> Lorem ipsum.</li>
						<li><strong>Amenities:</strong> Lorem ipsum.</li>
					</ul>
				</div>

				<div class="about">
					<h3 class="title large">Sobre o Imóvel</h3>	

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit perspiciatis quos doloribus commodi ea explicabo et consequuntur, at aperiam incidunt accusantium adipisci tempore, fugit similique. Et, repudiandae consequuntur consequatur deleniti.</p>				
				</div>

				<div class="maps">
					<h3 class="title large">Localização</h3>	

					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d60403.05823789234!2d-41.93487675!3d-18.8785998!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xb1a7947c2e06c7%3A0x76454399f628aa6f!2sGovernador+Valadares+-+MG%2C+Rep%C3%BAblica+Federativa+do+Brasil!5e0!3m2!1spt-BR!2s!4v1406429846810" width="100%" height="250" frameborder="0" style="border:0"></iframe>
				</div>
			</section>

			<aside class="sidebar">
				<div class="perfil">
					<h3 class="title-section large">Detalhes do Imóvel</h3>

					<figure class="thumbnail">
						<a href="" title=""><img src="http://placehold.it/222x120" alt=""></a>
					</figure>
					<div class="info">
						<h4 class="title-perfil"><a href="">Lorem ipsum dolor sit amet.</a></h4>
						<ul class="list">
							<li>Lorem ipsum dolor sit amet.</li>
							<li>Lorem ipsum dolor sit amet.</li>
							<li>Lorem ipsum dolor sit amet.</li>
						</ul>						
					</div>
				</div>

				<div class="form-more-info">
					<h3 class="title-section large">Detalhes do Imóvel</h3>
					<div class="wpcf7" id="wpcf7-f6-o1">
						<form action="/#wpcf7-f6-o1" method="post" class="wpcf7-form" novalidate="novalidate">
							<div style="display: none;">
								<input name="_wpcf7" value="6" type="hidden">
								<input name="_wpcf7_version" value="3.7.2" type="hidden">
								<input name="_wpcf7_locale" value="pt_BR" type="hidden">
								<input name="_wpcf7_unit_tag" value="wpcf7-f6-o1" type="hidden">
								<input name="_wpnonce" value="3c0ffaee02" type="hidden">
							</div>
							<p>
								<span class="wpcf7-form-control-wrap your-name">
								<input name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Nome" type="text">
								</span>
							</p>
							<p>
								<span class="wpcf7-form-control-wrap your-email">
								<input name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Email" type="email"></span>
							</p>
							<p>
								<span class="wpcf7-form-control-wrap your-message">
								<textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Testemunho"></textarea>
								</span>
							</p>
							<p><input value="Enviar" class="wpcf7-form-control wpcf7-submit" type="submit"><span style="visibility: hidden;" class="ajax-loader">&nbsp;</span></p>
							<div class="wpcf7-response-output wpcf7-display-none"></div>
						</form>
					</div>					
				</div>
			</aside>
		</section>

		<section class="similar-properties">
			<div class="container">
				<h3 class="title large">Imóveis Similares</h3>
				<div class="list-cards">
					<div class="card">
						<figure class="thumbnail">
							<a href="" title="">
								<img src="http://placehold.it/222x150" alt="">
								<figcaption class="code">código - 109752</figcaption>
							</a>
						</figure>

						<div class="info">
							<a href="" title="">
								<div class="type">Casa</div>
								<div class="distrito">Jardim do Trevo</div>
								<div class="city">Governador Valadares</div>
								<div class="price">R$ 1,00</div>					
							</a>

							<div class="more-info icon-property">
								<span class="icon toilet">2</span>
								<span class="icon room">3</span>
								<span class="icon dormitory">1</span>
							</div>
						</div>
					</div>	

					<div class="card"><figure class="thumbnail"><a href="" title=""><img src="http://placehold.it/222x150" alt=""><figcaption class="code">código - 109752</figcaption></a></figure><div class="info"><a href="" title=""><div class="type">Casa</div><div class="distrito">Jardim do Trevo</div><div class="city">Governador Valadares</div><div class="price">R$ 1,00</div></a><div class="more-info icon-property"><span class="icon toilet">2</span><span class="icon room">3</span><span class="icon dormitory">1</span></div></div></div><div class="card"><figure class="thumbnail"><a href="" title=""><img src="http://placehold.it/222x150" alt=""><figcaption class="code">código - 109752</figcaption></a></figure><div class="info"><a href="" title=""><div class="type">Casa</div><div class="distrito">Jardim do Trevo</div><div class="city">Governador Valadares</div><div class="price">R$ 1,00</div></a><div class="more-info icon-property"><span class="icon toilet">2</span><span class="icon room">3</span><span class="icon dormitory">1</span></div></div></div><div class="card"><figure class="thumbnail"><a href="" title=""><img src="http://placehold.it/222x150" alt=""><figcaption class="code">código - 109752</figcaption></a></figure><div class="info"><a href="" title=""><div class="type">Casa</div><div class="distrito">Jardim do Trevo</div><div class="city">Governador Valadares</div><div class="price">R$ 1,00</div></a><div class="more-info icon-property"><span class="icon toilet">2</span><span class="icon room">3</span><span class="icon dormitory">1</span></div></div></div>			
				</div>
			</div>
		</section>


		<footer class="footer container">
			<div class="branding-footer"><a href="" title=""><img src="assets/images/branding-footer.png" height="50" width="144" alt=""></a></div>

			<ul class="social">
				<li class="icon facebook"><a href="" title="facebook">Facebook</a></li>
				<li class="icon twitter"><a href="" title="twitter">Twitter</a></li>
			</ul>

			<div class="newsletter">
				<form action="">
					<label for="">Imóveis para você</label>
					<input type="text" placeholder="Digite seu e-mail">
					<input type="submit" value="ok">
				</form>
			</div>
		</footer>
	</div><!-- wrapper -->

	<script src="assets/javascripts/scripts.js"></script>
</body>
</html>		