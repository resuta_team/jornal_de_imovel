<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>

	<link rel="stylesheet" href="style.css">
</head>
<body>

	<div class="wrapper">
		<header class="header">
			<div class="container">
				<div class="branding"><a href="" title=""><img src="assets/images/branding.png" height="64" width="178" alt=""></a></div>

				<nav class="nav navigation-header">
					<ul id="menu-menu-principal" class="menu"><li id="menu-item-5" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-5"><a href="http://agrosb.apikiprojeto.com/">Home</a></li>
						<li id="menu-item-429" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-429"><a href="http://agrosb.apikiprojeto.com/home/a-agro-sb/">A Empresa</a>
						<li id="menu-item-263" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-263"><a href="http://agrosb.apikiprojeto.com/noticias/">Notícias</a></li>
						<li id="menu-item-239" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-239"><a href="http://agrosb.apikiprojeto.com/comercial/">Comercial</a>
						<ul class="sub-menu">
							<li id="menu-item-548" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-548"><a href="http://agrosb.apikiprojeto.com/leiloes-e-feiras/">Leilões e Feiras</a></li>
							<li id="menu-item-621" class="menu-item menu-item-type-taxonomy menu-item-object-agro_assets_for_sale menu-item-621"><a href="http://agrosb.apikiprojeto.com/bens-a-venda/venda-especiais/">Venda Direta</a></li>
							<li id="menu-item-978" class="menu-item menu-item-type-taxonomy menu-item-object-agro_assets_for_sale menu-item-978"><a href="http://agrosb.apikiprojeto.com/bens-a-venda/outros-produtos/">Outros Produtos</a></li>
							<li id="menu-item-612" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-612"><a href="http://www.bradesco.com.br/html/classic/produtos-servicos/outros/2-via-de-boleto.shtm">2ª via de boleto</a></li>
						</ul>
						</li>
						<li id="menu-item-550" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-550"><a href="http://agrosb.apikiprojeto.com/fale-conosco/">Fale Conosco</a></li>
					</ul>
				</nav>

				<div class="quick-search">
					<form action="">
						<label for="">Busca Rápida</label>
						<input type="text" placeholder="Código do Imóvel">
						<input type="submit" value="ok">
					</form>
				</div>
			</div>
		</header><!-- header -->

		<section class="entry-title">
			<div class="container">
				<h1 class="title">A Empresa</h1>
			</div>
		</section>

		<div class="container">
			<div class="content post hentry">
				<h1>h1 Lorem ipsum dolor sit amet.</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi debitis odio iure quisquam suscipit, vel. Eligendi quae totam fuga voluptate.</p>
				<h2>h2 Laboriosam perspiciatis reiciendis facere, hic!</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi debitis odio iure quisquam suscipit, vel. Eligendi quae totam fuga voluptate.</p>
				<h3>h3 Praesentium accusamus, neque hic corporis.</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi debitis odio iure quisquam suscipit, vel. Eligendi quae totam fuga voluptate.</p>
				<h4>h4 Perferendis sapiente adipisci, sed enim?</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi debitis odio iure quisquam suscipit, vel. Eligendi quae totam fuga voluptate.</p>
				<h5>h5 Architecto culpa sint expedita voluptate.</h5>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi debitis odio iure quisquam suscipit, vel. Eligendi quae totam fuga voluptate.</p>
				<h6>h6 Reiciendis officiis harum, assumenda! Nulla!</h6>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi debitis odio iure quisquam suscipit, vel. Eligendi quae totam fuga voluptate.</p>
				<a href="">title</a>
				<strong>Lorem ipsum dolor sit amet.</strong>
				<em>Lorem ipsum dolor.</em>

				<blockquote>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum accusantium, perferendis, inventore quisquam quas exercitationem aperiam a corporis vel facere illum optio natus eaque fuga, esse at. Quos, tenetur, natus.
				</blockquote>

				<ul>
					<li>Lorem ipsum dolor sit amet, consectetur.</li>
					<li>Lorem ipsum dolor sit amet, consectetur.</li>
					<li>Lorem ipsum dolor sit amet, consectetur.</li>
					<li>Lorem ipsum dolor sit amet, consectetur.</li>
				</ul>


				<ol>
					<li>Lorem ipsum dolor sit amet, consectetur.</li>
					<li>Lorem ipsum dolor sit amet, consectetur.</li>
					<li>Lorem ipsum dolor sit amet, consectetur.</li>
					<li>Lorem ipsum dolor sit amet, consectetur.</li>
				</ol>
			</div>

			<aside class="sidebar">
				<h3 class="title-section large">Imóveis Similares</h3>
				<div class="list-cards">
					<div class="card">
						<figure class="thumbnail">
							<a href="" title="">
								<img src="http://placehold.it/222x150" alt="">
								<figcaption class="code">código - 109752</figcaption>
							</a>
						</figure>

						<div class="info">
							<a href="" title="">
								<div class="type">Casa</div>
								<div class="distrito">Jardim do Trevo</div>
								<div class="city">Governador Valadares</div>
								<div class="price">R$ 1,00</div>
							</a>

							<div class="more-info icon-property">
								<span class="icon toilet">2</span>
								<span class="icon room">3</span>
								<span class="icon dormitory">1</span>
							</div>
						</div>
					</div>

					<div class="card">
						<figure class="thumbnail">
							<a href="" title="">
								<img src="http://placehold.it/222x150" alt="">
								<figcaption class="code">código - 109752</figcaption>
							</a>
						</figure>

						<div class="info">
							<a href="" title="">
								<div class="type">Casa</div>
								<div class="distrito">Jardim do Trevo</div>
								<div class="city">Governador Valadares</div>
								<div class="price">R$ 1,00</div>
							</a>

							<div class="more-info icon-property">
								<span class="icon toilet">2</span>
								<span class="icon room">3</span>
								<span class="icon dormitory">1</span>
							</div>
						</div>
					</div>
				</div>
			</aside>
		</div>

		<footer class="footer container">
			<div class="branding-footer"><a href="" title=""><img src="assets/images/branding-footer.png" height="50" width="144" alt=""></a></div>

			<ul class="social">
				<li class="icon facebook"><a href="" title="facebook">Facebook</a></li>
				<li class="icon twitter"><a href="" title="twitter">Twitter</a></li>
			</ul>

			<div class="newsletter">
				<form action="">
					<label for="">Imóveis para você</label>
					<input type="text" placeholder="Digite seu e-mail">
					<input type="submit" value="ok">
				</form>
			</div>
		</footer>
	</div><!-- wrapper -->

	<script src="assets/javascripts/scripts.js"></script>
</body>
</html>
