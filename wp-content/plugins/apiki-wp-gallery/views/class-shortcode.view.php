<?php
/**
 * Apiki WP Gallery Shortcode View
 *
 * @package Apiki WP Gallery
 * @subpackage Shortcode
 * @version 1.0
 */
class Apiki_WP_Gallery_Shortcode_View {

	public static function gallery_render( $attachments )
	{
		if ( ! is_array( $attachments ) )
			return;

		$first_model = $attachments[0];
		ob_start();
		?>
			<div class="wrap-media" data-component-gallery>
				<div class="media-slide-featured">
				
			    	<figure data-attr-figure class="media-slide-figure">
			    		<a href="javascript:void(0);" title="<?php echo esc_attr( $first_model->legend ); ?>">
			    			<img class="first" src="<?php echo esc_attr( $first_model->src_large ); ?>" alt="thumbnail-large">
				    		<figcaption class="caption">
				    			<?php echo esc_attr( $first_model->legend ); ?>
				    		</figcaption>
			    		</a>
			    	</figure>

			    	<div class="media-slide-counter">
						Imagem <span data-attr-counter>1</span> de <?php echo esc_attr( count( $attachments ) ); ?>
					</div>
				</div><!-- media-slide-featured -->

			    <div class="media-slide" data-attr-thumbs>
					<?php self::gallery_thumbnails_render( $attachments ); ?>
			    </div><!-- media-slide -->
		    </div><!-- wrap-media -->
		<?php
		$content = ob_get_contents();
		ob_end_clean();

		return $content;
	}

	public static function gallery_thumbnails_render( $attachments )
	{
		if ( ! is_array( $attachments ) )
			return;

		echo '<ul>';

		foreach ( $attachments as $key => $model ) :
			$large = array(
				'image'  => $model->src_large,
				'legend' => $model->legend,
			);

			?>
	    		<li <?php echo ( $key == 0 ) ? 'class="active"' : ''; ?>>
	    			<a href="javascript:void(0);" data-attr-index="<?php echo esc_attr( $key ); ?>" data-attr-image='<?php echo json_encode( $large ); ?>' title="<?php echo esc_attr( $model->legend ); ?>">
		    			<img width="155" src="<?php echo esc_url( $model->src_thumbnail ); ?>" alt="thumbnail">
		    		</a>
	    		</li>
			<?php
		endforeach;

		echo '</ul>';
	}
}
