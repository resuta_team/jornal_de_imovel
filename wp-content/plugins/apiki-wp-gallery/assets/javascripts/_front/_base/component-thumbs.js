Module("ApikiGallery.ComponentThumbs", function(ComponentThumbs) {

	ComponentThumbs.MAX_SLIDER = 5;
	ComponentThumbs.MIN_SLIDER = 3;

	ComponentThumbs.fn.initialize = function(container) {
		this.container = container;
		this.list      = this.container.find('> ul');
		this.on        = jQuery.proxy(this.container.on, this.container);
		this.emit      = jQuery.proxy(this.container.trigger, this.container);
		this._control  = null;
		this._listSize = 0;
	};

	ComponentThumbs.fn.init = function() {
		this._setPluginSlider();
		this._addEventListener();
	};

	ComponentThumbs.fn._getSliderOptions = function() {
		var options = {
			minSlides    	 : ComponentThumbs.MIN_SLIDER,
			maxSlides	 	 : ComponentThumbs.MAX_SLIDER,
			slideWidth	 	 : 170,
			slideMargin	     : 15,
			pager	 	     : false,
			infiniteLoop     : false,
			hideControlOnEnd : true,
			mode             : 'vertical'
		};

		return options;
	};

	ComponentThumbs.fn._setPluginSlider = function() {
		// Media slide
		if ( !jQuery.fn.bxSlider ) {
			return;
		}

		this._control  = this.list.bxSlider(this._getSliderOptions());
		this._listSize = this.list.find('[data-attr-image]').length;
	};

	ComponentThumbs.fn._onReloadPluginSlider = function(event) {
		this._control.reloadSlider(this._getSliderOptions());
	};

	ComponentThumbs.fn._addEventListener = function() {
		this.list
			.on('click', '[data-attr-image]', this._onClickItemThumb.bind(this))
		;

		ApikiGallery.Emitter
			.on( 'reload-thumbs', this._onReloadPluginSlider.bind(this))
		;
	};

	ComponentThumbs.fn._onClickItemThumb = function(event) {
		var model, index;
		var target    = this._getThumbByClick(event.target)
		var container = this._getContainerByThumb(target);

		if ( container.is('.active') ) {
			return;
		}

		model = target.data('attr-image');
		index = target.data('attr-index');

		this._actived(container);
		this.emit('click-thumb', [ target, model, index ]);
	};

	ComponentThumbs.fn.getThumbModelByIndex = function(index, forceActive) {
		var target = this.list.find('[data-attr-index="'+ index +'"]');

		if ( !target.length ) {
			return false;
		}

		if ( forceActive ) {
			this._actived(this._getContainerByThumb(target));
			this._moveSliderForIndex(index);
		}

		return target.data('attr-image');
	};

	ComponentThumbs.fn._moveSliderForIndex = function(index) {
		var indexSlider = Math.floor( index / ComponentThumbs.MAX_SLIDER );

		this._control.goToSlide(indexSlider);
	};

	ComponentThumbs.fn.getCurrentIndex = function() {
		var target = this.list.find('.active [data-attr-index]');

		if ( !target.length ) {
			return -1;
		}

		return parseInt( target.data('attr-index') );
	};

	ComponentThumbs.fn.getSize = function() {
		return parseInt( this._listSize );
	};

	ComponentThumbs.fn._getThumbByClick = function(elementDOM) {
		var target = jQuery( elementDOM );

		return ( target.is('[data-attr-image]') ) ? target : target.parents('[data-attr-image]');
	};

	ComponentThumbs.fn._getContainerByThumb = function(thumb) {
		var container = thumb.data('container-thumb');

		if ( !container ) {
			container = thumb.parent('li');
			thumb.data('container-thumb', container);
		}

		return container;
	};

	ComponentThumbs.fn._actived = function(container) {
		//remove all class active
		this.list
			.find('.active')
			.removeClass('active')
		;

		//active menu
		container.addClass('active');
	};
});
