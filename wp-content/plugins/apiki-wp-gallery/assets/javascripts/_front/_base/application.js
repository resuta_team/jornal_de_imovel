Module("ApikiGallery.Application", function(Application) {
	Application.fn.initialize = function(container) {
		ApikiGallery.FactoryComponent
			.create(container, 'Gallery', '[data-component-gallery]')
		;
	};
});
