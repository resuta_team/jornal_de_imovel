Module("ApikiGallery.Emitter", function(Emitter) {
	Emitter._    = jQuery({});
	Emitter.on   = jQuery.proxy( Emitter._, 'on' );
	Emitter.emit = jQuery.proxy( Emitter._, 'trigger' );
}, {});
