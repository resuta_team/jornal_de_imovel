Module("ApikiGallery.ComponentGallery", function(ComponentGallery) {
	ComponentGallery.fn.initialize = function(container) {
		this.container       = container;
		this.figure          = this.container.byData('attr-figure');
		//this.buttonNext      = this.container.byData('action-next');
		//this.buttonPrev      = this.container.byData('action-prev');
		this.displayCounter  = this.container.byData('attr-counter');
		this.thumbs          = ApikiGallery.ComponentThumbs(this.container.byData('attr-thumbs'));
	};

	ComponentGallery.fn.init = function() {
		this._startComponents();
		this._addEventListener();
		//this._disabledButtons();
	};

	ComponentGallery.fn._startComponents = function() {
		this.thumbs.init();
	};

	ComponentGallery.fn._addEventListener = function() {
		this.thumbs
			.on('click-thumb', this._onLoadFigure.bind(this))
		;

		// this.buttonNext
		// 	.on('click', this._onClickButtonNext.bind(this))
		// ;

		// this.buttonPrev
		// 	.on('click', this._onClickButtonPrev.bind(this))
		// ;
	};

	ComponentGallery.fn._onClickButtonNext = function() {
		this._move(1);
	};

	ComponentGallery.fn._onClickButtonPrev = function() {
		this._move(-1);
	};

	ComponentGallery.fn._move = function(operator) {
		var model;
		var current = ( this.thumbs.getCurrentIndex() + ( operator || 0 ) );

		//check disabled buttons
		//this._disabledButtons(current);

		if ( current < 0 || current >= this.thumbs.getSize() ) {
			return;
		}

		//get model
		model = this.thumbs.getThumbModelByIndex(current, true);
		this.setFigureCurrent(model);
		this.setCounter(current + 1);
	};

	ComponentGallery.fn._disabledButtons = function(current) {
		var current = ( typeof current == 'undefined' ? this.thumbs.getCurrentIndex() : current );
		this._resetButton();

		if ( current <= 0 ) {
			this.buttonPrev.addClass('disabled');
			return;
		}

		if ( current >= ( this.thumbs.getSize() - 1 ) ) {
			this.buttonNext.addClass('disabled');
			return;
		}
	};

	ComponentGallery.fn._resetButton = function() {
		this.buttonPrev.removeClass('disabled');
		this.buttonNext.removeClass('disabled');
	};

	ComponentGallery.fn._onLoadFigure = function(event, target, model, index) {
		this.setFigureCurrent(model);
		this.setCounter(index + 1);
		//this._disabledButtons(index);
	};

	ComponentGallery.fn.setCounter = function(index) {
		this.displayCounter.text(index);
	};

	ComponentGallery.fn.setFigureCurrent = function(model) {
		var template = '<a href="javascript:void(0);" title="{{legend}}">'
			    	 + '   <img src="{{image}}" alt="thumbnail-large">'
				     + '   <figcaption class="caption">{{legend}}</figcaption>'
			    	 + '</a>'
		;

		this.figure.html(this._buildTemplate(template, model));
	};

	ComponentGallery.fn._buildTemplate = function(html, model) {
		return html
			.replace(/\{\{image\}\}/g, model.image)
			.replace(/\{\{legend\}\}/g, model.legend)
		;
	};
});
