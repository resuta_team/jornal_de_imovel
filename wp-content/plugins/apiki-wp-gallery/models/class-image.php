<?php
/**
 * Apiki WP Gallery Image Model
 *
 * @package Apiki WP Gallery
 * @subpackage Image
 */
class Apiki_WP_Gallery_Image
{
	/**
	 * Meta attribute
	 *
	 * @since 1.0
	 * @var string
	 */
	private $ID;

	/**
	 * Src Large
	 *
	 * @since 1.0
	 * @var string
	 */
	private $src_large;

	/**
	 * Src Thumb
	 *
	 * @since 1.0
	 * @var string
	 */
	private $src_thumbnail;

	/**
	 * Legend
	 *
	 * @since 1.0
	 * @var string
	 */
	private $legend;

	/**
     * Constructor of the class. Instantiate and incializate it.
     *
     * @since 1.0.0
     *
     * @param int $ID - The ID of the Customer
     * @return null
     */
	public function __construct( $ID = false )
	{
		if ( false != $ID ) :
			$this->ID = $ID;
		endif;
	}

	/**
	 * Magic function to retrieve the value of the attribute more easily.
	 *
	 * @since 1.0
	 * @param string $prop_name The attribute name
	 * @return mixed The attribute value
	 */
	public function __get( $prop_name )
	{
		return $this->_get_property( $prop_name );
	}

	/**
	 * Magic function to set the value of the attribute more easily.
	 *
	 * @param string $prop_name The attribute name.
	 * @param string $value The attribute name.
	 * @since 1.0
	 */
	public function __set( $prop_name, $value )
	{
		return $this->_set_property( $prop_name, $value );
	}

	/**
	 * Use in __get() magic method to retrieve the value of the attribute
	 * on demand. If the attribute is unset get his value before.
	 *
	 * @since 1.0
	 * @param string $prop_name The attribute name
	 * @return mixed The value of the attribute
	 */
	private function _get_property( $prop_name )
	{
		switch ( $prop_name ) {
			case 'src_large' :
				$this->src_large = $this->_get_src_image( apply_filters( 'apiki_gallery_image_size_large', 'large' ) );
				break;

			case 'src_thumbnail' :
				$this->src_thumbnail = $this->_get_src_image( apply_filters( 'apiki_gallery_image_size_thumbnail', 'thumbnail' ) );
				break;

			case 'legend' :
				$this->legend = get_post_field( 'post_excerpt', $this->ID );
				break;
		}

		return $this->$prop_name;
	}

	private function _get_src_image( $image_size )
	{
		$attachment = wp_get_attachment_image_src( $this->ID, $image_size );

		if ( ! is_array( $attachment ) )
			return false;

		return $attachment[0];
	}
}
