<?php
/**
 * Apiki WP Gallery Shortcode Controller
 *
 * @package Apiki WP Gallery
 * @subpackage Shortcode
 * @since 1.0
 */
class Apiki_WP_Gallery_Shortcode_Controller {

	/**
	 * Instance of this class.
	 *
	 * @since 1.0
	 * @var object
	 */
	private static $instance = null;

	/**
	 * Adds needed actions to create submenu and page
	 *
	 * @since 1.0
	 * @return void
	 */
	private function __construct()
	{
		//remove_shortcode( 'gallery', 'gallery_shortcode' );
		add_shortcode( 'gallery', array( &$this, 'gallery_shortcode' ) );
	}

	public function gallery_shortcode( $attr )
	{
		$attachments = $this->query_gallery( $attr );

	    if ( empty( $attachments ) )
	        return false;

	   	//print script gallery
	   	wp_enqueue_script( Apiki_WP_Gallery::PLUGIN_SLUG . '-gallery-script' );

	   	$gallery = Apiki_WP_Gallery_Shortcode_View::gallery_render( $this->parse_model( $attachments ) );

	   	return $gallery;
	}

	/**
	 *
	 * This function handles the WordPress shortcode image gallery. Any configurations
	 * are considered. We just need the sortcode [gallery]
	 *
	 * @param array $attr Attributes attributed to the shortcode.
	 * @return string HTML content to display gallery.
	 */
	public function query_gallery( $attr )
	{
	    if ( isset( $attr['ids'] ) )
	    	return $this->_query_gallery_ids( $attr['ids'] );

	    return $this->_query_gallery_parent();
	}

	public function convert_posts_in_model( $post )
	{
		return new Apiki_WP_Gallery_Image( $post->ID );
	}

	public static function the_gallery( $post_id = '' )
	{
		global $post;

		if ( empty( $post_id ) )
			$post_id = $post->ID;

		self::the_extract_content_in_post( get_post_field( 'post_content', $post_id ) );
	}

	public static function the_extract_content_in_post( $post_content )
	{
		$matches = null;

		if ( ! has_shortcode( $post_content, 'gallery' ) )
			return;

		preg_match( '/(\[gallery.+\])/', $post_content,  $matches );

		if ( ! is_array( $matches ) )
			return;

		echo do_shortcode( $matches[1] );
	}

	private function parse_model( $attachments )
	{
		if ( empty( $attachments ) )
	        return false;

		return array_map( array( &$this, 'convert_posts_in_model' ), $attachments );
	}

	private function _query_gallery_ids( $ids )
	{
		if ( ! is_array( $ids ) ) :
			$ids = explode( ',', $ids );
		endif;

		$args = array(
			'post_type'      => 'attachment',
			'posts_per_page' => -1,
			'post__in'       => $ids,
			'orderby'        => 'menu_order',
			'order'          => 'ASC',
    	);

    	return get_posts( $args );
	}

	private function _query_gallery_parent()
	{
		global $post;

		$args = array(
			'post_parent'    => $post->ID,
			'post_status'    => 'inherit',
			'post_type'      => 'attachment',
			'post_mime_type' => 'image',
			'order'          => 'ASC',
			'orderby'        => 'menu_order',
	    );

	    return get_posts( $args );
	}

	/**
	 * Return an instance of this class.
	 *
	 * @since 1.0
	 * @return object A single instance of this class.
	 */
	public static function get_instance()
	{
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

}
