<?php
/*
Plugin Name: Apiki WP Gallery
Plugin URI: http://apiki.com
Description:
Version: 1.0
Author: Apiki WordPress
Author URI: http://apiki.com
License: GPL2
*/

spl_autoload_register( '__autoload_apiki_wp_gallery' );

/**
 * Autoload classes
 *
 * @param string $class_name
 * @return void
 */
function __autoload_apiki_wp_gallery( $class_name )
{
	$class_plugin = 'Apiki_WP_Gallery';

	if ( $class_name == $class_plugin ) :
		require_once sprintf( '%s/class-%s.php', dirname( __FILE__ ), str_replace( '_', '-', strtolower( $class_name ) ) );
		return;
	endif;

	if ( strpos( $class_name, $class_plugin ) === false )
		return;

	$class_name = strtolower( str_replace( $class_plugin . '_', '', $class_name ) );
	$class_name = explode( '_', $class_name );
	$class_type = ( isset( $class_name[1] ) ) ? $class_name[1] : 'model';

	switch ( $class_type ) {
		case 'controller' :
			require_once sprintf( '%s/controllers/class-%s.controller.php', dirname( __FILE__ ), $class_name[0] );
			break;
		case 'view' :
			require_once sprintf( '%s/views/class-%s.view.php', dirname( __FILE__ ), $class_name[0] );
			break;
		case 'model' :
			require_once sprintf( '%s/models/class-%s.php', dirname( __FILE__ ), $class_name[0] );
			break;
	}
}

add_action( 'plugins_loaded', array( 'Apiki_WP_Gallery', 'get_instance' ) );
