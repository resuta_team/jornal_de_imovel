<?php
/**
 * Apiki WP Gallery
 *
 * @package Apiki WP Gallery
 * @author  Apiki WordPress
 * @version 1.0
 */
class Apiki_WP_Gallery {

	/**
	 * Plugin version, used for cache-busting of style and script file references.
	 *
	 * @since 1.0
	 * @var string
	 */
	const VERSION = '1.0';

	/**
	 * Unique identifier for your plugin.
	 *
	 * Use this value (not the variable name) as the text domain
	 * when internationalizing strings of text. It should match
	 * the Text Domain file header in the main plugin file.
	 *
	 * @since 1.0
	 * @var string
	 */
	const PLUGIN_SLUG = 'apiki-wp-gallery';

	/**
	 * Instance of this class.
	 *
	 * @since 1.0
	 * @var object
	 */
	private static $instance = null;

	/**
	 * Initialize the plugin by setting localization, filters, and administration functions.
	 *
	 * @since 1.0
	 */
	private function __construct()
	{
		// Load public-facing style sheet and JavaScript.
		add_action( 'wp_enqueue_scripts', array( &$this, 'scripts_front' ) );

		Apiki_WP_Gallery_Shortcode_Controller::get_instance();
	}

	public function scripts_front()
	{
		wp_register_script(
			self::PLUGIN_SLUG . '-gallery-script',
			plugins_url( 'assets/javascripts/script.min.js', __FILE__ ),
			array( 'jquery' ),
			filemtime( plugin_dir_path(  __FILE__  ) . 'assets/javascripts/script.min.js' ),
			true
		);
	}

	/**
	 * Return an instance of this class.
	 *
	 * @since 1.0
	 * @return object A single instance of this class.
	 */
	public static function get_instance()
	{
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}
}
