<?php
/**
 * Address
 *
 * @package Resuta Manager
 * @subpackage Address
 */
class Resuta_Manager_Address
{
	/**
	 * property state
	 *
	 * @since 1.0
	 * @var string
	 */
	private $state;

	/**
	 * property city
	 *
	 * @since 1.0
	 * @var string
	 */
	private $city;

	/**
	 * property street
	 *
	 * @since 1.0
	 * @var int
	 */
	private $street;

	/**
	 * Post Metas
	 *
	 * @since 1.0
	 * @var string
	 */
	const POST_META_STREET = 'resuta_address_street';

	const POST_META_CITY = 'resuta_address_city';

	const POST_META_STATE = 'resuta_address_state';

	/**
     * Constructor of the class. Instantiate and incializate it.
     *
     * @since 1.0.0
     *
     * @param int $ID - The ID of the Customer
     * @return null
     */
	public function __construct( $ID = false )
	{
		if ( false != $ID ) :
			$this->ID = $ID;
		endif;
	}

	/**
	 * Magic function to retrieve the value of the attribute more easily.
	 *
	 * @since 1.0
	 * @param string $prop_name The attribute name
	 * @return mixed The attribute value
	 */
	public function __get( $prop_name )
	{
		return $this->_get_property( $prop_name );
	}

	public function get_city( $sufix = '' )
	{
		$id_city = $this->_get_property( 'city' );

		if ( empty( $id_city ) )
			return false;

		$controller = Resuta_Manager_Address_Controller::get_instance();
		$city       = $controller->get_city( $id_city );

		if ( is_array( $city ) && isset( $city['text'] ) )
			return $city['text'] . $sufix;

		return false;
	}

	public function get_state( $sufix = '' )
	{
		$id_state = $this->_get_property( 'state' );

		if ( empty( $id_state ) )
			return false;

		$controller = Resuta_Manager_Address_Controller::get_instance();
		$city       = $controller->get_state( $id_state );

		if ( is_array( $city ) && isset( $city['text'] ) )
			return $city['text'] . $sufix;

		return false;
	}

	public function get_name_district()
	{
		return Resuta_Manager_Utils_Helper::get_term_field(
			$this->ID,
			Resuta_Manager_Taxonomy_District_Controller::TAXONOMY,
			'name'
		);
	}

	public function get_slugs_district()
	{
		return Resuta_Manager_Utils_Helper::get_terms_field(
			$this->ID,
			Resuta_Manager_Taxonomy_District_Controller::TAXONOMY,
			'slug'
		);
	}

	public function get_pointer_fields()
	{
		$fields = array(
			self::POST_META_CITY  => $this->_get_property( 'city' ),
			self::POST_META_STATE => $this->_get_property( 'state' ),
		);

		return array_filter( $fields );
	}

	public function get_full_address()
	{
		$city     = $this->get_city();
		$state    = $this->get_state();
		$district = $this->get_name_district();
		$street   = $this->_get_property( 'street' );

		return "BRASIL, {$city} - {$state}, {$district}, {$street}";
	}

	public function get_address_for_map()
	{
		$city     = $this->get_city();
		$state    = $this->get_state();
		$district = $this->get_name_district();

		return "BRASIL, {$city} - {$state}, {$district}";
	}

	/**
	 * Use in __get() magic method to retrieve the value of the attribute
	 * on demand. If the attribute is unset get his value before.
	 *
	 * @since 1.0
	 * @param string $prop_name The attribute name
	 * @return mixed The value of the attribute
	 */
	private function _get_property( $prop_name )
	{
		switch ( $prop_name ) {
			case 'city' :
				if ( ! isset( $this->city ) ) :
					$this->city = get_post_meta( $this->ID, self::POST_META_CITY, true );
				endif;
				break;

			case 'state' :
				if ( ! isset( $this->state ) ) :
					$this->state = get_post_meta( $this->ID, self::POST_META_STATE, true );
				endif;
				break;

			case 'street' :
				if ( ! isset( $this->street ) ) :
					$this->street = get_post_meta( $this->ID, self::POST_META_STREET, true );
				endif;
				break;
		}

		return $this->$prop_name;
	}
}
