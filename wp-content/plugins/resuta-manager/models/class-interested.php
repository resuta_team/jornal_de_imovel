<?php
/**
 * Interested
 *
 * @package Resuta Manager
 * @subpackage Interested
 */
class Resuta_Manager_Interested
{
	/**
	 * property ID
	 *
	 * @since 1.0
	 * @var string
	 */
	private $ID;

	/**
	 * property title
	 *
	 * @since 1.0
	 * @var string
	 */
	private $title;

	/**
	 * property excerpt
	 *
	 * @since 1.0
	 * @var string
	 */
	private $excerpt;

	/**
	 * property email client
	 *
	 * @since 1.0
	 * @var string
	 */
	private $email;

	/**
	 * property phone client
	 *
	 * @since 1.0
	 * @var string
	 */
	private $phone;

	/**
	 * property price initial
	 *
	 * @since 1.0
	 * @var floaf
	 */
	private $price_initial;

	/**
	 * property price final
	 *
	 * @since 1.0
	 * @var float
	 */
	private $price_final;

	/**
	 * property address
	 *
	 * @since 1.0
	 * @var Resuta_Manager_Address
	 */
	private $address;

	/**
	 * Post Type name
	 *
	 * @since 1.0
	 * @var string
	 */
	const POST_TYPE = 'resuta_cpt_interest';

	/**
	 * Post Metas
	 *
	 * @since 1.0
	 * @var string
	 */
	const POST_META_PHONE = 'resuta_interested_phone';

	const POST_META_EMAIL = 'resuta_interested_email';

	const POST_META_PRICE_INITIAL = 'resuta_interested_price_initial';

	const POST_META_PRICE_FINAL = 'resuta_interested_price_final';

	/**
     * Constructor of the class. Instantiate and incializate it.
     *
     * @since 1.0.0
     *
     * @param int $ID - The ID of the Customer
     * @return null
     */
	public function __construct( $ID = false )
	{
		if ( false != $ID ) :
			$this->ID = $ID;
		endif;
	}

	/**
	 * Magic function to retrieve the value of the attribute more easily.
	 *
	 * @since 1.0
	 * @param string $prop_name The attribute name
	 * @return mixed The attribute value
	 */
	public function __get( $prop_name )
	{
		return $this->_get_property( $prop_name );
	}

	public function get_name_type()
	{
		return Resuta_Manager_Utils_Helper::get_term_field(
			$this->ID,
			Resuta_Manager_Taxonomy_Type_Controller::TAXONOMY,
			'name'
		);
	}

	public function get_name_transaction()
	{
		return Resuta_Manager_Utils_Helper::get_term_field(
			$this->ID,
			Resuta_Manager_Taxonomy_Transaction_Controller::TAXONOMY,
			'name'
		);
	}

	public function get_slugs_type()
	{
		return Resuta_Manager_Utils_Helper::get_terms_field(
			$this->ID,
			Resuta_Manager_Taxonomy_Type_Controller::TAXONOMY,
			'slug'
		);
	}

	public function get_slugs_transaction()
	{
		return Resuta_Manager_Utils_Helper::get_terms_field(
			$this->ID,
			Resuta_Manager_Taxonomy_Transaction_Controller::TAXONOMY,
			'slug'
		);
	}

	public function get_format_price_initial( $prefix = '' )
	{
		return $this->_get_format_price_by_field( $prefix, 'price_initial' );
	}

	public function get_format_price_final( $prefix = '' )
	{
		return $this->_get_format_price_by_field( $prefix, 'price_final' );
	}

	public function get_price_range()
	{
		$fields = array(
			self::POST_META_PRICE_INITIAL => $this->_get_property( 'price_initial' ),
			self::POST_META_PRICE_FINAL   => $this->_get_property( 'price_final' ),
		);

		return array_filter( $fields );
	}

	/**
	 * Use in __get() magic method to retrieve the value of the attribute
	 * on demand. If the attribute is unset get his value before.
	 *
	 * @since 1.0
	 * @param string $prop_name The attribute name
	 * @return mixed The value of the attribute
	 */
	private function _get_property( $prop_name )
	{
		switch ( $prop_name ) {
			case 'title' :
				if ( ! isset( $this->title ) ) :
					$this->title = get_post_field( 'post_title', $this->ID );
				endif;
				break;

			case 'excerpt' :
				if ( ! isset( $this->excerpt ) ) :
					$this->excerpt = get_post_field( 'post_excerpt', $this->ID );
				endif;
				break;

			case 'phone' :
				if ( ! isset( $this->phone ) ) :
					$this->phone = get_post_meta( $this->ID, self::POST_META_PHONE, true );
				endif;
				break;

			case 'email' :
				if ( ! isset( $this->email ) ) :
					$this->email = get_post_meta( $this->ID, self::POST_META_EMAIL, true );
				endif;
				break;

			case 'price_initial' :
				if ( ! isset( $this->price_initial ) ) :
					$this->price_initial = get_post_meta( $this->ID, self::POST_META_PRICE_INITIAL, true );
				endif;
				break;

			case 'price_final' :
				if ( ! isset( $this->price_final ) ) :
					$this->price_final = get_post_meta( $this->ID, self::POST_META_PRICE_FINAL, true );
				endif;
				break;

			case 'address' :
				if ( ! isset( $this->address ) ) :
					$this->address = new Resuta_Manager_Address( $this->ID );
				endif;
				break;
		}

		return $this->$prop_name;
	}

	public function _get_format_price_by_field( $prefix, $field )
	{
		$price = $this->_get_property( $field );

		return ( ! empty( $price ) ) ? $prefix . number_format( $price, 2, ',', '.' ) : '';
	}
}
