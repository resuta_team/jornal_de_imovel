<?php
/**
 * Property
 *
 * @package Resuta Manager
 * @subpackage Property
 */
class Resuta_Manager_Property
{
	/**
	 * property ID
	 *
	 * @since 1.0
	 * @var string
	 */
	private $ID;

	/**
	 * property title
	 *
	 * @since 1.0
	 * @var string
	 */
	private $title;

	/**
	 * property content
	 *
	 * @since 1.0
	 * @var string
	 */
	private $content;

	/**
	 * property price
	 *
	 * @since 1.0
	 * @var term
	 */
	private $price;

	/**
	 * property code
	 *
	 * @since 1.0
	 * @var string
	 */
	private $code;

	/**
	 * property type
	 *
	 * @since 1.0
	 * @var string
	 */
	private $type;

	/**
	 * property transaction
	 *
	 * @since 1.0
	 * @var string
	 */
	private $transaction;

	/**
	 * property attributes
	 *
	 * @since 1.0
	 * @var Resuta_Manager_Attributes
	 */
	private $attributes;

	/**
	 * property address
	 *
	 * @since 1.0
	 * @var Resuta_Manager_Address
	 */
	private $address;

	/**
	 * Post Type name
	 *
	 * @since 1.0
	 * @var string
	 */
	const POST_TYPE = 'resuta_cpt_property';

	/**
	 * Image size
	 *
	 * @since 1.0
	 * @var string
	 */
	const IMAGE_SIZE_MEDIUM = 'resuta_property_medium';

	const IMAGE_GALLERY_LARGE = 'resuta_property_gallery_large';

	const IMAGE_GALLERY_SMALL = 'resuta_property_gallery_small';

	/**
	 * Post Metas
	 *
	 * @since 1.0
	 * @var string
	 */
	const POST_META_CODE = 'resuta_property_code';

	const POST_META_PRICE = 'resuta_property_price';

	/**
     * Constructor of the class. Instantiate and incializate it.
     *
     * @since 1.0.0
     *
     * @param int $ID - The ID of the Customer
     * @return null
     */
	public function __construct( $ID = false )
	{
		if ( false != $ID ) :
			$this->ID = $ID;
		endif;
	}

	/**
	 * Magic function to retrieve the value of the attribute more easily.
	 *
	 * @since 1.0
	 * @param string $prop_name The attribute name
	 * @return mixed The attribute value
	 */
	public function __get( $prop_name )
	{
		return $this->_get_property( $prop_name );
	}

	public function get_format_price( $prefix = '' )
	{
		$price = $this->_get_property( 'price' );

		return ( ! empty( $price ) ) ? $prefix . number_format( $price, 2, ',', '.' ) : '';
	}

	public function get_name_type()
	{
		return Resuta_Manager_Utils_Helper::get_term_field(
			$this->ID,
			Resuta_Manager_Taxonomy_Type_Controller::TAXONOMY,
			'name'
		);
	}

	public function get_name_transaction()
	{
		return Resuta_Manager_Utils_Helper::get_term_field(
			$this->ID,
			Resuta_Manager_Taxonomy_Transaction_Controller::TAXONOMY,
			'name'
		);
	}

	public function get_slugs_type()
	{
		return Resuta_Manager_Utils_Helper::get_terms_field(
			$this->ID,
			Resuta_Manager_Taxonomy_Type_Controller::TAXONOMY,
			'slug'
		);
	}

	public function get_slugs_transaction()
	{
		return Resuta_Manager_Utils_Helper::get_terms_field(
			$this->ID,
			Resuta_Manager_Taxonomy_Transaction_Controller::TAXONOMY,
			'slug'
		);
	}

	public function has_gallery()
	{
		return has_shortcode( $this->_get_property( 'content' ), 'gallery' );
	}

	public function has_post_thumbnail()
	{
		return has_post_thumbnail( $this->ID );
	}

	public function get_the_post_thumbnail()
	{
		return get_the_post_thumbnail( $this->ID, Resuta_Manager_Property::IMAGE_SIZE_MEDIUM );
	}

	public function get_permalink()
	{
		return get_permalink( $this->ID );
	}

	/**
	 * Use in __get() magic method to retrieve the value of the attribute
	 * on demand. If the attribute is unset get his value before.
	 *
	 * @since 1.0
	 * @param string $prop_name The attribute name
	 * @return mixed The value of the attribute
	 */
	private function _get_property( $prop_name )
	{
		switch ( $prop_name ) {
			case 'title' :
				if ( ! isset( $this->title ) ) :
					$this->title = get_post_field( 'post_title', $this->ID );
				endif;
				break;

			case 'content' :
				if ( ! isset( $this->content ) ) :
					$this->content = get_post_field( 'post_content', $this->ID );
				endif;
				break;

			case 'price' :
				if ( ! isset( $this->price ) ) :
					$this->price = get_post_meta( $this->ID, self::POST_META_PRICE, true );
				endif;
				break;

			case 'code' :
				if ( ! isset( $this->code ) ) :
					$this->code = $this->_get_code();
				endif;
				break;

			case 'attributes' :
				if ( ! isset( $this->attributes ) ) :
					$this->attributes = new Resuta_Manager_Attributes( $this->ID );
				endif;
				break;

			case 'address' :
				if ( ! isset( $this->address ) ) :
					$this->address = new Resuta_Manager_Address( $this->ID );
				endif;
				break;
		}

		return $this->$prop_name;
	}

	private function _get_code()
	{
		$code_meta = get_post_meta( $this->ID, self::POST_META_CODE, true );

		if ( empty( $code_meta ) ) :
			$code_meta = $this->ID;
		endif;

		return $code_meta;
	}
}
