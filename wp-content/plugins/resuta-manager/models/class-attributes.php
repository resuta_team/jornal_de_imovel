<?php
/**
 * Attributes
 *
 * @package Resuta Manager
 * @subpackage Attributes
 */
class Resuta_Manager_Attributes
{
	/**
	 * property count of toilets
	 *
	 * @since 1.0
	 * @var int
	 */
	private $toilets;

	/**
	 * property count of rooms
	 *
	 * @since 1.0
	 * @var int
	 */
	private $rooms;

	/**
	 * property count of fourth
	 *
	 * @since 1.0
	 * @var int
	 */
	private $fourth;

	/**
	 * property street
	 *
	 * @since 1.0
	 * @var int
	 */
	private $street;

	/**
	 * property count of garage
	 *
	 * @since 1.0
	 * @var int
	 */
	private $garage;

	/**
	 * property count of crown
	 *
	 * @since 1.0
	 * @var int
	 */
	private $crown;

	/**
	 * property count of maid_dependence
	 *
	 * @since 1.0
	 * @var int
	 */
	private $maid_dependence;

	/**
	 * property count of maid_toilets
	 *
	 * @since 1.0
	 * @var int
	 */
	private $maid_toilets;

	/**
	 * property count of suite
	 *
	 * @since 1.0
	 * @var int
	 */
	private $suite;

	/**
	 * property count of service
	 *
	 * @since 1.0
	 * @var int
	 */
	private $service;

	/**
	 * property count of pantry
	 *
	 * @since 1.0
	 * @var int
	 */
	private $pantry;

	/**
	 * property count of balcony
	 *
	 * @since 1.0
	 * @var int
	 */
	private $balcony;

	/**
	 * property count of mezzanine
	 *
	 * @since 1.0
	 * @var int
	 */
	private $mezzanine;

	/**
	 * property count of office
	 *
	 * @since 1.0
	 * @var int
	 */
	private $office;

	/**
	 * property count of social toilets
	 *
	 * @since 1.0
	 * @var int
	 */
	private $social_toilets;

	/**
	 * property count of restroom
	 *
	 * @since 1.0
	 * @var int
	 */
	private $restroom;

	/**
	 * property count of bedroom closets
	 *
	 * @since 1.0
	 * @var int
	 */
	private $bedroom_closets;

	/**
	 * property count of bedroom toilets
	 *
	 * @since 1.0
	 * @var int
	 */
	private $bedroom_toilets;

	/**
	 * property count of bedroom kitchen
	 *
	 * @since 1.0
	 * @var int
	 */
	private $bedroom_kitchen;

	/**
	 * property count of backyard
	 *
	 * @since 1.0
	 * @var int
	 */
	private $backyard;

	/**
	 * property count of whirlpool
	 *
	 * @since 1.0
	 * @var int
	 */
	private $whirlpool;

	/**
	 * property count of closet
	 *
	 * @since 1.0
	 * @var int
	 */
	private $closet;

	/**
	 * property count of pool
	 *
	 * @since 1.0
	 * @var int
	 */
	private $pool;

	/**
	 * property count of gourmet
	 *
	 * @since 1.0
	 * @var int
	 */
	private $gourmet;

	/**
	 * property count of plane
	 *
	 * @since 1.0
	 * @var int
	 */
	private $plane;

	/**
	 * property count of walled
	 *
	 * @since 1.0
	 * @var int
	 */
	private $walled;

	/**
	 * Post Metas
	 *
	 * @since 1.0
	 * @var string
	 */

	//Banheiro
	const POST_META_TOILETS = 'resuta_attribute_toilets';
	//Sala
	const POST_META_ROOMS = 'resuta_attribute_rooms';
	//Quarto
	const POST_META_FOURTH = 'resuta_attribute_fourth';
	//Garagem
	const POST_META_GARAGE = 'resuta_attribute_garage';
	//Copa
	const POST_META_CROWN = 'resuta_attribute_crown';
	//Suíte
	const POST_META_SUITE = 'resuta_attribute_suite';
	//Área de serviço
	const POST_META_SERVICE = 'resuta_attribute_service';
	//Banheiro de empregada
	const POST_META_MAID_TOILETS = 'resuta_attribute_maid_toilets';
	//Dependência de empregada
	const POST_META_MAID_DEPENDENCE = 'resuta_attribute_maid_dependence';
	//Despensa
	const POST_META_PANTRY = 'resuta_attribute_pantry';
	//Sacada
	const POST_META_BALCONY = 'resuta_attribute_balcony';
	//Sobreloja
	const POST_META_MEZZANINE = 'resuta_attribute_mezzanine';
	//Escritório
	const POST_META_OFFICE = 'resuta_attribute_office';
	//Banheiro Social
	const POST_META_SOCIAL_TOILETS = 'resuta_attribute_social_toilets';
	//Lavabo
	const POST_META_RESTROOM = 'resuta_attribute_restroom';
	//Armários de quarto
	const POST_META_BEDROOM_CLOSETS = 'resuta_attribute_bedroom_closets';
	//Armários de banheiro
	const POST_META_BEDROOM_TOILETS = 'resuta_attribute_bedroom_toilets';
	//Armários de cozinha
	const POST_META_BEDROOM_KITCHEN = 'resuta_attribute_bedroom_kitchen';
	//Quintal
	const POST_META_BACKYARD = 'resuta_attribute_backyard';
	//Hidromassagem
	const POST_META_WHIRLPOOL = 'resuta_attribute_whirlpool';
	//Closet
	const POST_META_CLOSET = 'resuta_attribute_closet';
	//Piscina
	const POST_META_POOL = 'resuta_attribute_pool';
	//Área gourmet
	const POST_META_GOURMET = 'resuta_attribute_gourmet';
	//Plan
	const POST_META_PLANE = 'resuta_attribute_plane';
	//Plan
	const POST_META_WALLED = 'resuta_attribute_walled';

	/**
     * Constructor of the class. Instantiate and incializate it.
     *
     * @since 1.0.0
     *
     * @param int $ID - The ID of the Customer
     * @return null
     */
	public function __construct( $ID = false )
	{
		if ( false != $ID ) :
			$this->ID = $ID;
		endif;
	}

	/**
	 * Magic function to retrieve the value of the attribute more easily.
	 *
	 * @since 1.0
	 * @param string $prop_name The attribute name
	 * @return mixed The attribute value
	 */
	public function __get( $prop_name )
	{
		return $this->_get_property( $prop_name );
	}

	public function get_fields_checked()
	{
		$fields = array(
			self::POST_META_WALLED    => array( 'value' => $this->_get_property( 'walled' ), 'text' => 'Murado' ),
			self::POST_META_PLANE     => array( 'value' => $this->_get_property( 'plane' ), 'text' => 'Plano' ),
			self::POST_META_BACKYARD  => array( 'value' => $this->_get_property( 'backyard' ), 'text' => 'Quintal' ),
			self::POST_META_MEZZANINE => array( 'value' => $this->_get_property( 'mezzanine' ), 'text' => 'Sobreloja' ),
			self::POST_META_OFFICE    => array( 'value' => $this->_get_property( 'office' ), 'text' => 'Escritório' ),
			self::POST_META_WHIRLPOOL => array( 'value' => $this->_get_property( 'whirlpool' ), 'text' => 'Hidromassagem' ),
			self::POST_META_CLOSET    => array( 'value' => $this->_get_property( 'closet' ), 'text' => 'Closet' ),
			self::POST_META_POOL      => array( 'value' => $this->_get_property( 'pool' ), 'text' => 'Piscina' ),
			self::POST_META_GOURMET   => array( 'value' => $this->_get_property( 'gourmet' ), 'text' => 'Área Gourmet' ),
		);

		return $fields;
	}

	public function get_fields_number()
	{
		$fields = array(
			self::POST_META_GARAGE          => array( 'value' => $this->_get_property( 'garage' ), 'text' => 'Garagens' ),
			self::POST_META_TOILETS         => array( 'value' => $this->_get_property( 'toilets' ), 'text' => 'Banheiros' ),
			self::POST_META_ROOMS           => array( 'value' => $this->_get_property( 'rooms' ), 'text' => 'Salas' ),
			self::POST_META_FOURTH          => array( 'value' => $this->_get_property( 'fourth' ), 'text' => 'Quartos' ),
			self::POST_META_CROWN           => array( 'value' => $this->_get_property( 'crown' ), 'text' => 'Copas' ),
			self::POST_META_SUITE           => array( 'value' => $this->_get_property( 'suite' ), 'text' => 'Suítes' ),
			self::POST_META_MAID_TOILETS    => array( 'value' => $this->_get_property( 'maid_toilets' ), 'text' => 'Banheiros de Empregados' ),
			self::POST_META_MAID_DEPENDENCE => array( 'value' => $this->_get_property( 'maid_dependence' ), 'text' => 'Dependências de Empregados' ),
			self::POST_META_PANTRY          => array( 'value' => $this->_get_property( 'pantry' ), 'text' => 'Despensas' ),
			self::POST_META_BALCONY         => array( 'value' => $this->_get_property( 'balcony' ), 'text' => 'Sacadas' ),
			self::POST_META_SOCIAL_TOILETS  => array( 'value' => $this->_get_property( 'social_toilets' ), 'text' => 'Banheiros Sociais' ),
			self::POST_META_RESTROOM  	    => array( 'value' => $this->_get_property( 'restroom' ), 'text' => 'Lavabos' ),
			self::POST_META_BEDROOM_CLOSETS => array( 'value' => $this->_get_property( 'bedroom_closets' ), 'text' => 'Armários de Quartos' ),
			self::POST_META_BEDROOM_TOILETS => array( 'value' => $this->_get_property( 'bedroom_toilets' ), 'text' => 'Armários de Banheiros' ),
			self::POST_META_BEDROOM_KITCHEN => array( 'value' => $this->_get_property( 'bedroom_kitchen' ), 'text' => 'Armários de Cozinha' ),
			self::POST_META_SERVICE         => array( 'value' => $this->_get_property( 'service' ), 'text' => 'Áreas de Serviço' ),
		);

		return $fields;
	}

	public function get_all_fields( $is_remove_empty = true )
	{
		$fields = array_merge(
			$this->get_fields_number(),
			$this->get_fields_checked()
		);

		if ( ! $is_remove_empty )
			return $fields;

		return array_filter( $fields, array( 'Resuta_Manager_Utils_Helper', 'filter_array_value' ) );
	}

	/**
	 * Use in __get() magic method to retrieve the value of the attribute
	 * on demand. If the attribute is unset get his value before.
	 *
	 * @since 1.0
	 * @param string $prop_name The attribute name
	 * @return mixed The value of the attribute
	 */
	private function _get_property( $prop_name )
	{
		switch ( $prop_name ) {
			case 'toilets' :
				if ( ! isset( $this->toilets ) ) :
					$this->toilets = get_post_meta( $this->ID, self::POST_META_TOILETS, true );
				endif;
				break;

			case 'fourth' :
				if ( ! isset( $this->fourth ) ) :
					$this->fourth = get_post_meta( $this->ID, self::POST_META_FOURTH, true );
				endif;
				break;

			case 'garage' :
				if ( ! isset( $this->garage ) ) :
					$this->garage = get_post_meta( $this->ID, self::POST_META_GARAGE, true );
				endif;
				break;

			case 'rooms' :
				if ( ! isset( $this->rooms ) ) :
					$this->rooms = get_post_meta( $this->ID, self::POST_META_ROOMS, true );
				endif;
				break;

			case 'backyard' :
				if ( ! isset( $this->backyard ) ) :
					$this->backyard = get_post_meta( $this->ID, self::POST_META_BACKYARD, true );
				endif;
				break;

			case 'walled' :
				if ( ! isset( $this->walled ) ) :
					$this->walled = get_post_meta( $this->ID, self::POST_META_WALLED, true );
				endif;
				break;

			case 'plane' :
				if ( ! isset( $this->plane ) ) :
					$this->plane = get_post_meta( $this->ID, self::POST_META_PLANE, true );
				endif;
				break;

			case 'gourmet' :
				if ( ! isset( $this->gourmet ) ) :
					$this->gourmet = get_post_meta( $this->ID, self::POST_META_GOURMET, true );
				endif;
				break;

			case 'pool' :
				if ( ! isset( $this->pool ) ) :
					$this->pool = get_post_meta( $this->ID, self::POST_META_POOL, true );
				endif;
				break;

			case 'whirlpool' :
				if ( ! isset( $this->whirlpool ) ) :
					$this->whirlpool = get_post_meta( $this->ID, self::POST_META_WHIRLPOOL, true );
				endif;
				break;

			case 'restroom' :
				if ( ! isset( $this->restroom ) ) :
					$this->restroom = get_post_meta( $this->ID, self::POST_META_RESTROOM, true );
				endif;
				break;

			case 'closet' :
				if ( ! isset( $this->closet ) ) :
					$this->closet = get_post_meta( $this->ID, self::POST_META_CLOSET, true );
				endif;
				break;

			case 'office' :
				if ( ! isset( $this->office ) ) :
					$this->office = get_post_meta( $this->ID, self::POST_META_OFFICE, true );
				endif;
				break;

			case 'mezzanine' :
				if ( ! isset( $this->mezzanine ) ) :
					$this->mezzanine = get_post_meta( $this->ID, self::POST_META_MEZZANINE, true );
				endif;
				break;

			case 'balcony' :
				if ( ! isset( $this->balcony ) ) :
					$this->balcony = get_post_meta( $this->ID, self::POST_META_BALCONY, true );
				endif;
				break;

			case 'pantry' :
				if ( ! isset( $this->pantry ) ) :
					$this->pantry = get_post_meta( $this->ID, self::POST_META_PANTRY, true );
				endif;
				break;

			case 'suite' :
				if ( ! isset( $this->suite ) ) :
					$this->suite = get_post_meta( $this->ID, self::POST_META_SUITE, true );
				endif;
				break;

			case 'bedroom_closets' :
				if ( ! isset( $this->bedroom_closets ) ) :
					$this->bedroom_closets = get_post_meta( $this->ID, self::POST_META_BEDROOM_CLOSETS, true );
				endif;
				break;

			case 'bedroom_kitchen' :
				if ( ! isset( $this->bedroom_kitchen ) ) :
					$this->bedroom_kitchen = get_post_meta( $this->ID, self::POST_META_BEDROOM_KITCHEN, true );
				endif;
				break;

			case 'bedroom_toilets' :
				if ( ! isset( $this->bedroom_toilets ) ) :
					$this->bedroom_toilets = get_post_meta( $this->ID, self::POST_META_BEDROOM_TOILETS, true );
				endif;
				break;

			case 'social_toilets' :
				if ( ! isset( $this->social_toilets ) ) :
					$this->social_toilets = get_post_meta( $this->ID, self::POST_META_SOCIAL_TOILETS, true );
				endif;
				break;

			case 'crown' :
				if ( ! isset( $this->crown ) ) :
					$this->crown = get_post_meta( $this->ID, self::POST_META_CROWN, true );
				endif;
				break;

			case 'service' :
				if ( ! isset( $this->service ) ) :
					$this->service = get_post_meta( $this->ID, self::POST_META_SERVICE, true );
				endif;
				break;

			case 'maid_dependence' :
				if ( ! isset( $this->maid_dependence ) ) :
					$this->maid_dependence = get_post_meta( $this->ID, self::POST_META_MAID_DEPENDENCE, true );
				endif;
				break;

			case 'maid_toilets' :
				if ( ! isset( $this->maid_toilets ) ) :
					$this->maid_toilets = get_post_meta( $this->ID, self::POST_META_MAID_TOILETS, true );
				endif;
				break;
		}

		return $this->$prop_name;
	}
}
