<?php
/**
 * Enterprise
 *
 * @package Resuta Manager
 * @subpackage Enterprise
 */
class Resuta_Manager_Enterprise
{
	/**
	 * Answers ID
	 *
	 * @since 1.0
	 * @var string
	 */
	private $ID;

	/**
	 * Answers Title
	 *
	 * @since 1.0
	 * @var string
	 */
	private $display_name;

	/**
	 * Description enterprise
	 *
	 * @since 1.0
	 * @var string
	 */
	private $description;

	/**
	 * Answers Title
	 *
	 * @since 1.0
	 * @var string
	 */
	private $email;

	/**
	 * Status Title
	 *
	 * @since 1.0
	 * @var string
	 */
	private $city;

	/**
	 * Slug Title
	 *
	 * @since 1.0
	 * @var string
	 */
	private $state;

	/**
	 * Slug CNPJ
	 *
	 * @since 1.0
	 * @var string
	 */
	private $street;

	/**
	 * Slug Address
	 *
	 * @since 1.0
	 * @var string
	 */
	private $phones;

	/**
	 * Slug Avatar ID
	 *
	 * @since 1.0
	 * @var string
	 */
	private $avatar_id;

	/**
	 * Type enterprise
	 *
	 * @since 1.0
	 * @var string
	 */
	private $type;

	/**
	 * Plan type
	 *
	 * @since 1.0
	 * @var string
	 */
	// private $plan_type;

	/**
	 * Role Enterprises
	 *
	 * @since 1.0
	 * @var string
	 */
	const ROLE = 'resuta_role_enterprises';

	/**
	 * User Metas
	 *
	 * @since 1.0
	 * @var string
	 */
	const USER_META_CITY = 'resuta_enterprises_city';

	const USER_META_STATE = 'resuta_enterprises_state';

	const USER_META_STREET = 'resuta_enterprises_street';

	const USER_META_PHONES = 'resuta_enterprises_phones';

	const USER_META_AVATAR = 'resuta_enterprises_avatar';

	const USER_META_TYPE = 'resuta_enterprises_type';

	/**
	 * Options Type
	 *
	 * @since 1.0
	 * @var string
	 */
	const OPTION_TYPE_AGENT = 'agent';

	const OPTION_TYPE_REAL_ESTATE = 'real_estate';

	const OPTION_TYPE_PHYSICAL_PERSON = 'physical_person';

	/**
	 * Image Size Avatar
	 *
	 * @since 1.0
	 * @var string
	 */

	const IMAGE_SIZE_AVATAR_SMALL = 'resuta_enterprises_avatar_small';

	/**
     * Constructor of the class. Instantiate and incializate it.
     *
     * @since 1.0.0
     *
     * @param int $ID - The ID of the Customer
     * @return null
     */
	public function __construct( $ID = false )
	{
		if ( false != $ID ) :
			$this->ID = $ID;
		endif;
	}

	/**
	 * Magic function to retrieve the value of the attribute more easily.
	 *
	 * @since 1.0
	 * @param string $prop_name The attribute name
	 * @return mixed The attribute value
	 */
	public function __get( $prop_name )
	{
		return $this->_get_property( $prop_name );
	}

	public function get_avatar_url_small()
	{
		return $this->get_image( self::IMAGE_SIZE_AVATAR_SMALL );
	}

	public function get_image( $image_size, $default = '' )
	{
		$avatar_id  = $this->_get_property( 'avatar_id' );

		$attachment = wp_get_attachment_image_src( $avatar_id, $image_size );

		if ( $attachment )
			return $attachment[0];

		return $default;
	}

	public function get_text_type()
	{
		$type = $this->_get_property( 'type' );
		$args = array(
			self::OPTION_TYPE_AGENT           => 'Corretor',
			self::OPTION_TYPE_REAL_ESTATE     => 'Imobiliária',
			self::OPTION_TYPE_PHYSICAL_PERSON => 'P. Física'
		);

		return $args[$type];
	}

	public function get_phones_espace()
	{
		$phones = $this->_get_property( 'phones' );

		if ( empty( $phones ) )
			return false;

		return preg_replace( '/\,/', ', ', $phones );
	}

	/**
	 * Use in __get() magic method to retrieve the value of the attribute
	 * on demand. If the attribute is unset get his value before.
	 *
	 * @since 1.0
	 * @param string $prop_name The attribute name
	 * @return mixed The value of the attribute
	 */
	private function _get_property( $prop_name )
	{
		switch ( $prop_name ) {
			case 'ID' :
				return $this->ID;

			case 'display_name' :
				if ( ! isset( $this->display_name ) ) :
					$this->display_name = get_the_author_meta( 'display_name', $this->ID );
				endif;
				break;

			case 'description' :
				if ( ! isset( $this->description ) ) :
					$this->description = get_the_author_meta( 'description', $this->ID );
				endif;
				break;

			case 'email' :
				if ( ! isset( $this->email ) ) :
					$this->email = get_the_author_meta( 'user_email', $this->ID );
				endif;
				break;

			case 'street' :
				if ( ! isset( $this->street ) ) :
					$this->street = get_the_author_meta( self::USER_META_STREET, $this->ID );
				endif;
				break;

			case 'city' :
				if ( ! isset( $this->city ) ) :
					$this->city = get_the_author_meta( self::USER_META_CITY, $this->ID );
				endif;
				break;

			case 'state' :
				if ( ! isset( $this->state ) ) :
					$this->state = get_the_author_meta( self::USER_META_STATE, $this->ID );
				endif;
				break;

			case 'avatar_id' :
				if ( ! isset( $this->avatar_id ) ) :
					$this->avatar_id = get_the_author_meta( self::USER_META_AVATAR, $this->ID );
				endif;
				break;

			case 'phones' :
				if ( ! isset( $this->phones ) ) :
					$this->phones = get_the_author_meta( self::USER_META_PHONES, $this->ID );
				endif;
				break;

			case 'type' :
				if ( ! isset( $this->type ) ) :
					$type        = get_the_author_meta( self::USER_META_TYPE, $this->ID );
					$this->type  = ( empty( $type ) ) ? self::OPTION_TYPE_AGENT : $type;
				endif;
				break;

			default :
				return false;
				break;
		}

		return $this->$prop_name;
	}
}
