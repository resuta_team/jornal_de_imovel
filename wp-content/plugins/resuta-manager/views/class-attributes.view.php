<?php
/**
 * Views Attributes
 *
 * @package Resuta Manager
 * @subpackage Attributes Views
 * @version 1.0
 */
class Resuta_Manager_Attributes_View
{
	public static function render_qualifications_checked( Resuta_Manager_Attributes $model )
	{
		$fields = $model->get_fields_checked();

		foreach ( $fields as $key => $item ) :
			?>
			<p>
				<label for="<?php echo esc_attr( $key ); ?>">
					<input name="resuta[<?php echo esc_attr( $key ); ?>]" type="checkbox"
					       id="<?php echo esc_attr( $key ); ?>"
					       <?php checked( 1, intval( $item['value'] ) ); ?> value="1"><?php echo esc_html( $item['text'] ); ?>
				</label>
			</p>
			<?php
		endforeach;
	}

	public static function render_qualifications_number( Resuta_Manager_Attributes $model )
	{
		$fields = $model->get_fields_number();

		foreach ( $fields as $key => $item ) :
			?>
			<p>
				<label for="<?php echo esc_attr( $key ); ?>"><?php echo esc_html( $item['text'] ); ?></label><br>
				<input id="<?php echo esc_attr( $key ); ?>" type="number" class="medium-text"
					   name="resuta[<?php echo esc_attr( $key ); ?>]" value="<?php echo intval( $item['value'] ); ?>">
			</p>
			<?php
		endforeach;
	}

	public static function render_qualifications_number_front( Resuta_Manager_Attributes $model )
	{
		$fields = $model->get_fields_number();

		foreach ( $fields as $field ) :
			if ( (bool)$field['value'] )
				echo "<li><strong>{$field['text']}:</strong> {$field['value']}</li>";
		endforeach;
	}

	public static function render_qualifications_checked_front( Resuta_Manager_Attributes $model )
	{
		$fields = $model->get_fields_checked();

		foreach ( $fields as $field ) :
			if ( (bool)$field['value'] )
				echo "<li><strong>{$field['text']}:</strong> Sim</li>";
		endforeach;
	}
}
