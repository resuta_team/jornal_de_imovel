<?php
/**
 * Views Address
 *
 * @package Resuta Manager
 * @subpackage Address Views
 * @version 1.0
 */
class Resuta_Manager_Address_View
{
	public static function render_street( Resuta_Manager_Address $model )
	{
		?>
		<p>
			<label for="resuta-street">Rua/Logradouro</label><br>
			<input id="resuta-street" type="text" class="large-text" placeholder="Ex.: Rua de exemplo, numero"
			       name="resuta[<?php echo esc_attr( Resuta_Manager_Address::POST_META_STREET ); ?>]" value="<?php echo esc_attr( $model->street ); ?>">
		</p>
		<?php
	}

	public static function render_selects( Resuta_Manager_Address $model )
	{
		$address     = Resuta_Manager_Address_Controller::get_instance();
		$list_states = $address->get_list_single_states( $model->state );
		$list_cities = $address->get_list_single_cities( $model->city, $model->state );

		?>
			<div class="wrap-state-city" data-component-address>
				<p>
					<label for="resuta-state">Estado</label><br>
					<select id="resuta-state" class="select-chosen-full" data-attr-states
					        name="resuta[<?php echo esc_attr( Resuta_Manager_Address::POST_META_STATE ); ?>]">
						<?php self::render_options( $list_states, $model->state ); ?>
					</select>
				</p>
				<p>
					<label for="resuta-city">Cidade</label><br>
					<select id="resuta-city" class="select-chosen-full" data-attr-cities
					        name="resuta[<?php echo esc_attr( Resuta_Manager_Address::POST_META_CITY ); ?>]">
						<?php self::render_options( $list_cities, $model->city ); ?>
					</select>
				</p>
			</div>
		<?php
	}

	public static function render_options( $list, $current )
	{
		foreach ( $list as $item ) :
			?>
				<option value="<?php echo esc_attr( $item['value'] ) ?>" <?php selected( $item['value'], $current ); ?>><?php echo esc_html( $item['text'] ) ?></option>
			<?php
		endforeach;
	}
}
