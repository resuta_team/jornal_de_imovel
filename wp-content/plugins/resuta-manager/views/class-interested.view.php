<?php
/**
 * Views Interested
 *
 * @package Resuta Manager
 * @subpackage Interested Views
 * @version 1.0
 */
class Resuta_Manager_Interested_View
{
	public static function render_info_property( $post )
	{
		$model = new Resuta_Manager_Interested( $post->ID );

		?>
			<?php Resuta_Manager_Address_View::render_selects( $model->address ); ?>
			<div>
				<p>
					<label>Preço</label><br>
					<input id="resuta-price-initial" type="text" class="medium-text" placeholder="Ex.: 10.000,00"
					       data-mask-maxlength="false" data-mask-reverse="true" data-mask="#.##0,00"
					       name="resuta[<?php echo esc_attr( Resuta_Manager_Interested::POST_META_PRICE_INITIAL ); ?>]"
					       value="<?php echo esc_attr( $model->get_format_price_initial() ); ?>">

					<span>Até</span>

					<input id="resuta-price-initial" type="text" class="medium-text" placeholder="Ex.: 50.000,00"
					       data-mask-maxlength="false" data-mask-reverse="true" data-mask="#.##0,00"
					       name="resuta[<?php echo esc_attr( Resuta_Manager_Interested::POST_META_PRICE_FINAL ); ?>]"
					       value="<?php echo esc_attr( $model->get_format_price_final() ); ?>">
				</p>
			</div>
		<?php

		wp_nonce_field(
			Resuta_Manager_Interested_Controller::NONCE_INFO_PROPERTY_ACTION,
			Resuta_Manager_Interested_Controller::NONCE_INFO_PROPERTY_NAME
		);
	}

	public static function render_info_client( $post )
	{
		$model = new Resuta_Manager_Interested( $post->ID );

		?>
			<div>
				<p>
					<label for="resuta-name-client">Nome</label><br>
					<input id="resuta-name-client" type="text" class="large-text"
					       name="post_title" value="<?php echo esc_attr( $post->post_title ); ?>">
				</p>
				<p>
					<label for="resuta-phone-client">Telefones</label><br>
					<input id="resuta-phone-client" type="text" class="medium-text input-tags-phone"
					       name="resuta[<?php echo esc_attr( Resuta_Manager_Interested::POST_META_PHONE ); ?>]" value="<?php echo esc_attr( $model->phone ); ?>">

					<span class="description">Informe todos os telefones de contato do seu cliente, ao terminar de digitar cada telefone pressione "enter".</span>
				</p>
				<p>
					<label for="resuta-email-client">Email</label><br>
					<input id="resuta-email-client" type="text" class="large-text"
					       name="resuta[<?php echo esc_attr( Resuta_Manager_Interested::POST_META_EMAIL ); ?>]" value="<?php echo esc_attr( $model->email ); ?>">
				</p>
				<p>
					<label for="resuta-comments-client">Observações</label><br>
					<textarea id="resuta-comments-client" rows="5" cols="40"
					          class="large-text" name="excerpt"><?php echo htmlspecialchars_decode( $post->post_excerpt ); ?></textarea>
				</p>
			</div>
		<?php

		wp_nonce_field(
			Resuta_Manager_Interested_Controller::NONCE_INFO_CLIENT_ACTION,
			Resuta_Manager_Interested_Controller::NONCE_INFO_CLIENT_NAME
		);
	}

	public static function render_search_property( $post )
	{
		if ( $post->post_status != 'publish' ) :
			self::render_search_no_config();
			return;
		endif;

		$model      = new Resuta_Manager_Interested( $post->ID );
		$controller = Resuta_Manager_Property_Controller::get_instance();
		$list       = $controller->get_list_by_interested( $model );

		if ( ! $list ) :
			self::render_search_no_results();
			return;
		endif;

		self::render_search_results( $list );
	}

	public static function render_search_results( $list )
	{
		$alternate = 'class="alternate"';

		?>
		<table class="wp-list-table widefat list-interested-property">
			<thead>
				<tr>
					<th scope="col">Imagem</th>
					<th scope="col">Tipo do Imóvel</th>
					<th scope="col">Cidade</th>
					<th scope="col">Bairro</th>
					<th scope="col">Valor</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ( $list as $index => $model ) : ?>
					<tr <?php echo ( $index % 2 ) ? $alternate : ''; ?> scope="row">
						<td class="thumbnail-property">
							<?php self::render_thumbnail_image_property( $model ); ?>
						</td>
						<td>
							<?php echo esc_html( $model->get_name_type() ); ?>
						</td>
						<td>
							<?php echo esc_html( $model->address->get_city( ', ' ) . $model->address->get_state() ); ?>
						</td>
						<td>
							<?php echo esc_html( $model->address->get_name_district() ); ?>
						</td>
						<td>
							<?php echo esc_html( $model->get_format_price( 'R$ ' ) ); ?>
						</td>
					</tr>
				 <?php endforeach; ?>
			</tbody>
		</table>
		<?php
	}

	public static function render_thumbnail_image_property( Resuta_Manager_Property $model )
	{
		if ( ! $model->has_post_thumbnail() ) :
			echo '<p>Sem imagem<p>';
			return;
		endif;

		?>
			<a href="<?php echo esc_url( $model->get_permalink() ) ?>" title="<?php echo esc_attr( $model->title ); ?>" target="_blank">
				<?php echo $model->get_the_post_thumbnail(); ?>
			</a>
		<?php
	}

	public static function render_search_no_config()
	{
		?>
			<p>Sem configurações para realizar sua busca.</p>
		<?php
	}

	public static function render_search_no_results()
	{
		?>
			<p>Ainda não encontramos nenhum imóvel com essas configurações.</p>
		<?php
	}
}
