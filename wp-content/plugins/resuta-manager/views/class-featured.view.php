<?php
/**
 * Views Featured
 *
 * @package Resuta Manager
 * @subpackage Featured Views
 * @version 1.0
 */
class Resuta_Manager_Featured_View
{
	public static function render_link_control( $post )
	{
		$model = new Resuta_Manager_Featured( $post->ID );

		?>
			<p>
				<input id="resuta-field-link" type="text" class="large-text" placeholder="http://"
				       name="resuta[<?php echo esc_attr( Resuta_Manager_Featured::POST_META_LINK ); ?>]"
				       value="<?php echo esc_url( $model->link ); ?>">

			</p>
		<?php

		wp_nonce_field(
			Resuta_Manager_Featured_Controller::NONCE_LINK_ACTION, 
			Resuta_Manager_Featured_Controller::NONCE_LINK_NAME
		);
	}
}
