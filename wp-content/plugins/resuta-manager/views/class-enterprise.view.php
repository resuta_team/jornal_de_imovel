<?php
/**
 * Views Enterprise
 *
 * @package Resuta Manager
 * @subpackage Enterprise Views
 * @version 1.0
 */
class Resuta_Manager_Enterprise_View
{
	public static function render_info_property_dashboard()
	{
		$model = new Resuta_Manager_Enterprise( get_current_user_id() );

		?>
			<div class="welcome-panel dashboard-widget info-property">
				<img src="<?php echo esc_url( Resuta_Manager::path_assets( 'images/branding.png' ) ); ?>" alt="logo da empresa">
				<div class="welcome-panel-column-container">
					<div class="welcome-panel-column">
						<h4>Vamos começar, clique no botão abaixo</h4>
						<a class="button button-primary button-hero hide-if-customize" href="<?php echo esc_url( Resuta_Manager_Property_Controller::admin_url_create() ); ?>">Cadastrar novo imóvel</a>
					</div>
					<div class="welcome-panel-column welcome-panel-last">
						<h4>Mais ações</h4>
						<ul>
							<li>
								<div class="welcome-icon welcome-widgets-menus">Gerencie seus <a href="<?php echo esc_url( Resuta_Manager_Property_Controller::admin_url_list() ); ?>">imóveis</a></div>
							</li>
							<li>
								<a href="<?php echo esc_url( admin_url( 'profile.php' ) ); ?>" class="welcome-icon dashicons-admin-users">Mantenha seus dados atualizados</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		<?php
	}

	public static function render_profile_enterprise( $user_id )
	{
		$model = new Resuta_Manager_Enterprise( $user_id );

		?>
			<h3>Dados de Configuração</h3>
			<table class="form-table enterprises-table">
				<tbody>
					<tr>
						<th scope="row">
							<label for="resuta-corporate-name">Nome de Exibição</label>
						</th>
						<td>
							<input type="text" name="display_name"
							       id="resuta-corporate-name" value="<?php echo esc_attr( $model->display_name ); ?>"
							       class="large-text" data-not-remove>
						</td>
					</tr>
					<tr>
						<th scope="row">
							<label for="resuta-phones">Telefones</label>
						</th>
						<td>
							<input type="text" name="<?php echo esc_attr( Resuta_Manager_Enterprise::USER_META_PHONES ); ?>"
								   id="resuta-phones" value="<?php echo esc_attr( $model->phones ); ?>"
								   class="medium-text input-tags-phone">
						</td>
					</tr>
					<tr>
						<th scope="row">
							<label>Tipo de cadastro</label>
						</th>
						<td>
							<fieldset>
								<label title="Corretor">
									<input type="radio" name="<?php echo esc_attr( Resuta_Manager_Enterprise::USER_META_TYPE ); ?>"
									       value="<?php echo esc_attr( Resuta_Manager_Enterprise::OPTION_TYPE_AGENT ); ?>"
									       <?php checked( Resuta_Manager_Enterprise::OPTION_TYPE_AGENT, $model->type ); ?>>
									<span>Corretor</span>
								</label>
								<br>
								<label title="Imobiliária">
									<input type="radio" name="<?php echo esc_attr( Resuta_Manager_Enterprise::USER_META_TYPE ); ?>"
									       value="<?php echo esc_attr( Resuta_Manager_Enterprise::OPTION_TYPE_REAL_ESTATE ); ?>"
									       <?php checked( Resuta_Manager_Enterprise::OPTION_TYPE_REAL_ESTATE, $model->type ); ?>>
									<span>Imobiliária</span>
								</label>
								<br>
								<label title="P. Física">
									<input type="radio" name="<?php echo esc_attr( Resuta_Manager_Enterprise::USER_META_TYPE ); ?>"
									       value="<?php echo esc_attr( Resuta_Manager_Enterprise::OPTION_TYPE_PHYSICAL_PERSON ); ?>"
									       <?php checked( Resuta_Manager_Enterprise::OPTION_TYPE_PHYSICAL_PERSON, $model->type ); ?>>
									<span>P. Física</span>
								</label>
								<br>
							</fieldset>
						</td>
					</tr>
					<tr>
						<th scope="row">
							<label>Foto ou Logo</label>
						</th>
						<td>
							<button class="r-image-select" type="button"
					        		data-component-upload-file
							        data-attr-button-text="Selecionar imagem"
							        data-attr-image-position="inner"
							        data-attr-image-src="<?php echo esc_url( $model->get_avatar_url_small() ); ?>"
							        data-attr-hidden-name="<?php echo esc_attr( Resuta_Manager_Enterprise::USER_META_AVATAR ); ?>"
							        data-attr-hidden-value="<?php echo esc_attr( $model->avatar_id ); ?>">
					    		<span class="dashicons dashicons-format-image"></span>
								<span class="description">Clique aqui para selecionar sua imagem</span>
							</button>
						</td>
					</tr>
				</tbody>
			</table>
		<?php
	}

	public static function render_email_password( Resuta_Manager_Enterprise $model, $pass )
	{
		$admin_email = get_option( 'admin_email' );

		ob_start()
		?>
			<p>Olá, <?php echo esc_html( $model->display_name ); ?></p>
			<p>Segue abaixo seus dados de acesso:</p>
			<ul>
				<li>
					<strong>Email </strong>
					<?php echo esc_html( $model->email ); ?>
				</li>
				<li>
					<strong>Senha </strong>
					<?php echo esc_html( $pass ); ?>
				</li>
			</ul>
			<p>Sistema Jornal de Imóvel | <?php echo site_url(); ?> | <a href="mailto:<?php echo esc_attr( $admin_email ) ?>"><?php echo esc_attr( $admin_email ) ?></a></p>
		<?php
		$content = ob_get_contents();
		ob_end_clean();

		return $content;
	}
}
