<?php
/**
 * Views Property
 *
 * @package Resuta Manager
 * @subpackage Property Views
 * @version 1.0
 */
class Resuta_Manager_Property_View
{
	public static function render_price_code( $post )
	{
		$model = new Resuta_Manager_Property( $post->ID );

		?>
			<div>
				<p>
					<label for="resuta-price">Preço</label><br>
					<input id="resuta-price" type="text" class="large-text" placeholder="Ex.: 10.000,00"
					       data-mask-maxlength="false" data-mask-reverse="true" data-mask="#.##0,00"
					       name="resuta[<?php echo esc_attr( Resuta_Manager_Property::POST_META_PRICE ); ?>]" value="<?php echo esc_attr( $model->get_format_price() ); ?>">
				</p>
				<p>
					<label for="resuta-code">Código</label><br>
					<input id="resuta-code" type="text" class="large-text"
					       name="resuta[<?php echo esc_attr( Resuta_Manager_Property::POST_META_CODE ); ?>]" value="<?php echo esc_attr( $model->code ); ?>">
				</p>
			</div>
		<?php

		wp_nonce_field(
			Resuta_Manager_Property_Controller::NONCE_ADDICIONAL_ACTION,
			Resuta_Manager_Property_Controller::NONCE_ADDICIONAL_NAME
		);
	}

	public static function render_info( $post )
	{
		$model = new Resuta_Manager_Property( $post->ID );

		?>
			<?php Resuta_Manager_Address_View::render_selects( $model->address ); ?>
			<div>
				<?php Resuta_Manager_Address_View::render_street( $model->address ); ?>
			</div>
			<div class="wrap-left">
				<?php Resuta_Manager_Attributes_View::render_qualifications_checked( $model->attributes ); ?>
			</div>
			<div class="wrap-left">
				<?php Resuta_Manager_Attributes_View::render_qualifications_number( $model->attributes ); ?>
			</div>
		<?php

		wp_nonce_field(
			Resuta_Manager_Property_Controller::NONCE_INFO_ACTION,
			Resuta_Manager_Property_Controller::NONCE_INFO_NAME
		);
	}
}
