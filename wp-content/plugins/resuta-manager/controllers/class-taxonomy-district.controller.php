<?php
/**
 * Controller Taxonomy District
 *
 * @package Resuta Manager
 * @subpackage Taxonomy District
 * @since 1.0
 */
class Resuta_Manager_Taxonomy_District_Controller
{
	/**
	 * Instance of this class.
	 *
	 * @since 1.0
	 * @var object
	 */
	protected static $instance = null;

	/**
	 * Return an instance of this class.
	 *
	 * @since 1.0
	 * @return object A single instance of this class.
	 */
	public static function get_instance()
	{
		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Tanoxonomy name
	 *
	 * @since 1.0
	 * @var string
	 */
	const TAXONOMY = 'resuta_tax_district';

	/**
	 * Adds needed actions after plugin in enabled
	 *
	 * @since 1.0
	 * @return void
	 */
	public function __construct()
	{
		add_action( 'init', array( &$this, 'register_taxonomy' ), 0 );
	}

	/**
	 * Register all taxonomies of class
	 *
	 * @return void
	 */
	public function register_taxonomy()
	{
		register_taxonomy(
			self::TAXONOMY,
			NULL,
			array(
				'labels'        => array(
					'name'              => 'Bairro',
					'singular_name'     => 'Bairro',
					'menu_name'         => 'Bairro',
					'all_items'         => 'Todos os bairros',
					'edit_item'         => 'Editar bairro',
					'view_item'         => 'Visualizar bairro',
					'update_item'       => 'Atualizar bairro',
					'add_new_item'      => 'Adicionar novo bairro',
					'new_item_name'     => 'Nome do novo bairro',
					'parent_item'       => 'Bairro pai',
					'parent_item_colon' => 'Bairro pai:',
					'search_items'      => 'Pesquisar bairros',
					'popular_items'     => 'Bairros populares',
				),
				'public'            => true,
				'show_tagcloud'     => false,
				'show_admin_column' => true,
				'hierarchical'      => true,
				'rewrite'			=> array( 'slug' => 'bairro', 'with_front' => false ),
				'capabilities'      => array( 'assign_terms' => 'assign_terms_' . self::TAXONOMY ),
			)
		);
	}
}
