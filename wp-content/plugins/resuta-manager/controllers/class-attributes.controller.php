<?php
/**
 * Controller Attributes
 *
 * @package Resuta Manager
 * @subpackage Attributes
 * @since 1.0
 */
class Resuta_Manager_Attributes_Controller
{
	/**
	 * Instance of this class.
	 *
	 * @since 1.0
	 * @var object
	 */
	protected static $instance = null;

	/**
	 * Adds needed actions to create submenu and page
	 *
	 * @since 1.0
	 * @return void
	 */
	public function __construct()
	{
		add_action( 'resuta_' . Resuta_Manager_Property::POST_TYPE . '_after_save_metas', array( &$this, 'set_default_fields_checked' ), 10, 2 );
		add_action( 'resuta_' . Resuta_Manager_Interested::POST_TYPE . '_after_save_metas', array( &$this, 'set_default_fields_checked' ), 10, 2 );
	}

	public function set_default_fields_checked( $fields, $post_id )
	{
		$fields_checked = array(
			Resuta_Manager_Attributes::POST_META_WALLED,
			Resuta_Manager_Attributes::POST_META_PLANE,
			Resuta_Manager_Attributes::POST_META_BACKYARD,
			Resuta_Manager_Attributes::POST_META_MEZZANINE,
			Resuta_Manager_Attributes::POST_META_OFFICE,
			Resuta_Manager_Attributes::POST_META_WHIRLPOOL,
			Resuta_Manager_Attributes::POST_META_CLOSET,
			Resuta_Manager_Attributes::POST_META_POOL,
			Resuta_Manager_Attributes::POST_META_GOURMET,
		);

		foreach ( $fields_checked as $key ) :
			( ! array_key_exists( $key, $fields ) && update_post_meta( $post_id, $key, 0 ) );
		endforeach;
	}

	/**
	 * Return an instance of this class.
	 *
	 * @since 1.0
	 * @return object A single instance of this class.
	 */
	public static function get_instance()
	{
		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}
}
