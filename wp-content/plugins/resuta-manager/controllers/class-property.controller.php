<?php
/**
 * Controller Property
 *
 * @package Resuta Manager
 * @subpackage Property
 * @since 1.0
 */
class Resuta_Manager_Property_Controller
{
	/**
	 * Instance of this class.
	 *
	 * @since 1.0
	 * @var object
	 */
	protected static $instance = null;

	/**
	 * Nonce Action
	 *
	 * @since 1.0
	 * @var object
	 */
	const NONCE_INFO_ACTION = '_resuta_property_info_action';

	const NONCE_ADDICIONAL_ACTION = '_resuta_property_addicional_action';

	/**
	 * Nonce Name
	 *
	 * @since 1.0
	 * @var object
	 */
	const NONCE_INFO_NAME = '_resuta_property_info_name';

	const NONCE_ADDICIONAL_NAME = '_resuta_property_addicional_name';

	/**
	 * Nonce Name
	 *
	 * @since 1.0
	 * @var object
	 */
	const PRICE_SIMILAR = 20000;

	/**
	 * Adds needed actions to create submenu and page
	 *
	 * @since 1.0
	 * @return void
	 */
	public function __construct()
	{
		add_action( 'init', array( &$this, 'register_post_type' ) );
		add_action( 'after_setup_theme', array( &$this, 'define_image_sizes' ) );
		add_action( 'add_meta_boxes', array( &$this, 'define_metaboxes' ) );
		add_action( 'pre_get_posts', array( &$this, 'customize_query' ) );
		add_filter( 'resuta_' . Resuta_Manager_Property::POST_TYPE . '_is_valid_save_post', array( &$this, 'nonce_valid_save_post' ) );
		add_filter( 'resuta_' . Resuta_Manager_Property::POST_TYPE . '_save_value', array( &$this, 'filter_save_price' ), 10, 2 );
		add_filter( 'apiki_gallery_image_size_large', array( &$this, 'size_image_large' ) );
		add_filter( 'apiki_gallery_image_size_thumbnail', array( &$this, 'size_image_thumbnail' ) );
		add_action( 'pre_get_posts', array( &$this, 'customize_admin_query' ) );
	}

	public function customize_admin_query( $query )
	{
		if ( ! is_admin() )
			return;

		//set author user logged
		if ( current_user_can( Resuta_Manager_Enterprise::ROLE ) && $query->query_vars[ 'post_type' ] == 'attachment' )
			$query->set( 'author', get_current_user_id() );

		//set author user logged
		if ( current_user_can( Resuta_Manager_Enterprise::ROLE ) && $query->is_main_query() )
			$query->set( 'author', get_current_user_id() );
	}

	public function size_image_large( $size )
	{
		return Resuta_Manager_Property::IMAGE_GALLERY_LARGE;
	}

	public function size_image_thumbnail( $size )
	{
		return Resuta_Manager_Property::IMAGE_GALLERY_SMALL;
	}

	public function customize_query( $query )
	{
		if ( is_admin() )
			return;

		if ( $query->is_author() ) :
			$query->set( 'post_type', Resuta_Manager_Property::POST_TYPE );
		endif;

		if ( $query->is_post_type_archive( Resuta_Manager_Property::POST_TYPE ) && $query->is_main_query() ) :
			$this->_insert_meta_in_query( $args );
			$this->_insert_taxonomy_in_query( $args );
			$args = array_merge( $query->query, $args );

			$query->parse_query( $args );
		endif;
	}

	public function define_metaboxes()
	{
		add_meta_box(
			'resuta-metabox-property-info',
			'Informações do Imóvel',
			array( 'Resuta_Manager_Property_View', 'render_info' ),
			Resuta_Manager_Property::POST_TYPE,
			'normal',
			'high'
		);

		add_meta_box(
			'resuta-metabox-property-addicional',
			'Preço e Código',
			array( 'Resuta_Manager_Property_View', 'render_price_code' ),
			Resuta_Manager_Property::POST_TYPE,
			'side',
			'high'
		);
	}

	public function get_list_district()
	{
		return $this->_get_list_by_taxonomy( Resuta_Manager_Taxonomy_District_Controller::TAXONOMY );
	}

	public function get_list_type()
	{
		return $this->_get_list_by_taxonomy( Resuta_Manager_Taxonomy_Type_Controller::TAXONOMY );
	}

	public function get_list_transaction()
	{
		return $this->_get_list_by_taxonomy( Resuta_Manager_Taxonomy_Transaction_Controller::TAXONOMY );
	}

	public function filter_save_price( $value, $key )
	{
		if ( $key == Resuta_Manager_Property::POST_META_PRICE )
			$value = Resuta_Manager_Utils_Helper::convert_float_for_sql( $value );

		return $value;
	}

	public function nonce_valid_save_post( $is_valid )
	{
		$info_nonce = Resuta_Manager_Utils_Helper::post_method_params( self::NONCE_INFO_NAME, false );
		$add_nonce  = Resuta_Manager_Utils_Helper::post_method_params( self::NONCE_ADDICIONAL_NAME, false );

		if ( ! $info_nonce || ! wp_verify_nonce( $info_nonce, self::NONCE_INFO_ACTION ) )
			return false;

		if ( ! $add_nonce || ! wp_verify_nonce( $add_nonce, self::NONCE_ADDICIONAL_ACTION ) )
			return false;

		return true;
	}

	public function get_list( $args = array(), $is_parse_list = true, $filter_default_params = true )
	{
		$defaults = array(
			'post_type' => Resuta_Manager_Property::POST_TYPE,
		);

		//include params url
		if ( $filter_default_params ) :
			$this->_insert_meta_in_query( $args );
			$this->_insert_taxonomy_in_query( $args );
		endif;

		$args  = wp_parse_args( $args, $defaults );
		$query = Resuta_Manager_Utils_Helper::get_query( $args );

		if ( ! $is_parse_list )
			return $query;

		return $this->_parse_list( $query );
	}

	public function get_list_by_interested( Resuta_Manager_Interested $model )
	{
		$args              = array();
		$taxonomies_filter = $this->merge_taxonomies_by_interested( $model );
		$address_filter    = $model->address->get_pointer_fields();
		$price_filter      = $model->get_price_range();

		$this->_insert_filters_metas( $address_filter, $args );
		$this->_insert_filters_taxonomies( $taxonomies_filter, $args );

		if ( $price_filter ) :
			$args['meta_query'][] = array(
				'key'     => Resuta_Manager_Property::POST_META_PRICE,
				'value'   => $price_filter,
				'compare' => 'BETWEEN',
				'type'    => 'DECIMAL'
			);
		endif;

		return $this->get_list( $args, true, false );
	}

	public function merge_taxonomies_by_interested( Resuta_Manager_Interested $model )
	{
		$taxonomies = array(
			Resuta_Manager_Taxonomy_Type_Controller::TAXONOMY        => $model->get_slugs_type(),
			Resuta_Manager_Taxonomy_Transaction_Controller::TAXONOMY => $model->get_slugs_transaction(),
			Resuta_Manager_Taxonomy_District_Controller::TAXONOMY    => $model->address->get_slugs_district(),
		);

		return array_filter( $taxonomies );
	}

	public function get_list_by_price( $id_property = 0, $is_parse_list = true )
	{
		$model     = new Resuta_Manager_Property( $id_property );
		$price_max = $model->price + self::PRICE_SIMILAR;
		$price_min = max( $model->price - self::PRICE_SIMILAR, 0 );
		unset( $model );

		$args = array(
			'post_type'      => Resuta_Manager_Property::POST_TYPE,
			'post__not_in'   => array( $id_property ),
			'posts_per_page' => 8,
			'meta_query'     => array(
				array(
					'key'     => Resuta_Manager_Property::POST_META_PRICE,
					'value'   => array( $price_min, $price_max ),
					'type'    => 'NUMERIC',
					'compare' => 'BETWEEN',
				),
			)
		);

		$query = Resuta_Manager_Utils_Helper::get_query( $args );

		if ( ! $is_parse_list )
			return $query;

		return $this->_parse_list( $query );
	}

	public function register_post_type()
	{
		register_post_type(
			Resuta_Manager_Property::POST_TYPE,
			array(
				'labels' => array(
					'name'               => 'Imóveis',
					'singular_name'      => 'Imóvel',
					'all_items'          => 'Todos os imóveis',
					'add_new'            => 'Adicionar novo',
					'add_new_item'       => 'Adicionar novo imóvel',
					'edit_item'          => 'Editar imóvel',
					'new_item'           => 'Novo imóvel',
					'view_item'          => 'Visualizar imóveis',
					'not_found_in_trash' => 'Nenhum imóvel encontrado na lixeira',
				),
				'public'        	=> true,
				'has_archive'       => true,
				'menu_position' 	=> 5,
				'supports'      	=> array( 'title', 'editor', 'thumbnail', 'author' ),
				'rewrite'           => array( 'slug' => 'imoveis', 'with_front' => false ),
				'taxonomies'        => array(
					Resuta_Manager_Taxonomy_Type_Controller::TAXONOMY,
					Resuta_Manager_Taxonomy_District_Controller::TAXONOMY,
					Resuta_Manager_Taxonomy_Transaction_Controller::TAXONOMY,
				),
				'menu_icon'			=> 'dashicons-location-alt',
				'capability_type'	=> Resuta_Manager_Property::POST_TYPE,
			)
		);
	}

	public function define_image_sizes()
	{
		//controller image
		$controller_image = Resuta_Manager_Image_Controller::get_instance();

		$controller_image->define(
			Resuta_Manager_Property::POST_TYPE,
			array(
				Resuta_Manager_Property::IMAGE_SIZE_MEDIUM   => array( 222, 150, true ),
				Resuta_Manager_Property::IMAGE_GALLERY_LARGE => array( 510, 345, true ),
				Resuta_Manager_Property::IMAGE_GALLERY_SMALL => array( 155, 105, true ),
			)
		);
	}

	/**
	 * Return an instance of this class.
	 *
	 * @since 1.0
	 * @return object A single instance of this class.
	 */
	public static function get_instance()
	{
		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public static function admin_url_create()
	{
		return admin_url( 'post-new.php?post_type=' . Resuta_Manager_Property::POST_TYPE );
	}

	public static function admin_url_list()
	{
		return admin_url( 'edit.php?post_type=' . Resuta_Manager_Property::POST_TYPE );
	}

	private function _parse_list( $wp_query )
	{
		if ( ! $wp_query->have_posts() )
			return false;

		$list = array();

		foreach ( $wp_query->posts as $diary )
			$list[] = new Resuta_Manager_Property( $diary->ID );

		return $list;
	}

	private function _get_list_by_taxonomy( $taxonomy )
	{
		$terms = get_terms( $taxonomy );
		$list  = array();

		if ( is_wp_error( $terms ) || ! is_array( $terms ) )
			return false;

		foreach ( $terms as $term ) :
			$list[] = array(
				'value' => $term->slug,
				'text'  => $term->name,
			);
		endforeach;

		return $list;
	}

	private function _insert_taxonomy_in_query( &$args )
	{
		$district    = Resuta_Manager_Utils_Helper::get_method_params( 'district', false );
		$type        = Resuta_Manager_Utils_Helper::get_method_params( 'type', false );
		$transaction = Resuta_Manager_Utils_Helper::get_method_params( 'transaction', Resuta_Manager_Taxonomy_Transaction_Controller::OPTION_SALE );
		$code        = Resuta_Manager_Utils_Helper::get_method_params( 'code', false );

		if ( $code )
			return;

		if ( $district ) :
			$args['tax_query'][] = array(
				'taxonomy' => Resuta_Manager_Taxonomy_District_Controller::TAXONOMY,
				'field'    => 'slug',
				'terms'    => sanitize_title( $district ),
			);
		endif;

		if ( $type ) :
			$args['tax_query'][] = array(
				'taxonomy' => Resuta_Manager_Taxonomy_Type_Controller::TAXONOMY,
				'field'    => 'slug',
				'terms'    => sanitize_title( $type ),
			);
		endif;

		if ( $transaction ) :
			$args['tax_query'][] = array(
				'taxonomy' => Resuta_Manager_Taxonomy_Transaction_Controller::TAXONOMY,
				'field'    => 'slug',
				'terms'    => sanitize_title( $transaction ),
			);
		endif;
	}

	private function _insert_meta_in_query( &$args )
	{
		$state = Resuta_Manager_Utils_Helper::get_method_params( 'state', false );
		$city  = Resuta_Manager_Utils_Helper::get_method_params( 'city', false );
		$code  = Resuta_Manager_Utils_Helper::get_method_params( 'code', false );

		if ( $state ) :
			$args['meta_query'][] = array(
				'key'   => Resuta_Manager_Address::POST_META_STATE,
				'value' => intval( $state ),
			);
		endif;

		if ( $city ) :
			$args['meta_query'][] = array(
				'key'   => Resuta_Manager_Address::POST_META_CITY,
				'value' => intval( $city ),
			);
		endif;

		if ( $code ) :
			$args['meta_query'][] = array(
				'key'   => Resuta_Manager_Property::POST_META_CODE,
				'value' => $code,
			);
		endif;
	}

	private function _insert_filters_metas( $filters, &$args )
	{
		foreach ( $filters as $key => $value ) :
			$args['meta_query'][] = array(
				'key'     => $key,
				'value'   => esc_html( $value ),
			);
		endforeach;
	}

	private function _insert_filters_taxonomies( $filters, &$args )
	{
		foreach ( $filters as $key => $value ) :
			$args['tax_query'][] = array(
				'taxonomy' => $key,
				'terms'    => $value,
				'field'	   => 'slug',
			);
		endforeach;
	}
}
