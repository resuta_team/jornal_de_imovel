<?php
/**
 * Controller Taxonomy Type
 *
 * @package Resuta Manager
 * @subpackage Taxonomy Type
 * @since 1.0
 */
class Resuta_Manager_Taxonomy_Type_Controller
{
	/**
	 * Instance of this class.
	 *
	 * @since 1.0
	 * @var object
	 */
	protected static $instance = null;

	/**
	 * Return an instance of this class.
	 *
	 * @since 1.0
	 * @return object A single instance of this class.
	 */
	public static function get_instance()
	{
		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Tanoxonomy name
	 *
	 * @since 1.0
	 * @var string
	 */
	const TAXONOMY = 'resuta_tax_type';

	/**
	 * Adds needed actions after plugin in enabled
	 *
	 * @since 1.0
	 * @return void
	 */
	public function __construct()
	{
		add_action( 'init', array( &$this, 'register_taxonomy' ), 0 );
	}

	/**
	 * Register all taxonomies of class
	 *
	 * @return void
	 */
	public function register_taxonomy()
	{
		register_taxonomy(
			self::TAXONOMY,
			NULL,
			array(
				'labels'        => array(
					'name'              => 'Tipo de Imóvel',
					'singular_name'     => 'Tipo de Imóvel',
					'menu_name'         => 'Tipo de Imóvel',
					'all_items'         => 'Todos os tipos',
					'edit_item'         => 'Editar tipo de imóvel',
					'view_item'         => 'Visualizar tipo de imóvel',
					'update_item'       => 'Atualizar tipo de imóvel',
					'add_new_item'      => 'Adicionar novo tipo de imóvel',
					'new_item_name'     => 'Nome do novo tipo de imóvel',
					'parent_item'       => 'Tipo de imóvel pai',
					'parent_item_colon' => 'Tipo de imóvel pai:',
					'search_items'      => 'Pesquisar tipos de imóveis',
					'popular_items'     => 'Tipos de imóveis populares',
				),
				'public'            => true,
				'show_tagcloud'     => false,
				'show_admin_column' => true,
				'hierarchical'      => true,
				'rewrite'			=> array( 'slug' => 'tipo-de-imovel', 'with_front' => false ),
				'capabilities'      => array( 'assign_terms' => 'assign_terms_' . self::TAXONOMY ),
			)
		);
	}
}
