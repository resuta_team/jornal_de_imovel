<?php
/**
 * Controller Interested
 *
 * @package Resuta Manager
 * @subpackage Interested
 * @since 1.0
 */
class Resuta_Manager_Interested_Controller
{
	/**
	 * Instance of this class.
	 *
	 * @since 1.0
	 * @var object
	 */
	protected static $instance = null;

	/**
	 * Nonce Action
	 *
	 * @since 1.0
	 * @var object
	 */
	const NONCE_INFO_PROPERTY_ACTION = '_resuta_interested_info_property_action';

	const NONCE_INFO_CLIENT_ACTION = '_resuta_interested_info_client_action';

	/**
	 * Nonce Name
	 *
	 * @since 1.0
	 * @var object
	 */
	const NONCE_INFO_PROPERTY_NAME = '_resuta_interested_info_property_name';

	const NONCE_INFO_CLIENT_NAME = '_resuta_interested_info_client_name';

	/**
	 * Adds needed actions to create submenu and page
	 *
	 * @since 1.0
	 * @return void
	 */
	public function __construct()
	{
		add_action( 'init', array( &$this, 'register_post_type' ) );
		add_action( 'add_meta_boxes', array( &$this, 'define_metaboxes' ) );
		add_filter( 'resuta_' . Resuta_Manager_Interested::POST_TYPE . '_is_valid_save_post', array( &$this, 'nonce_valid_save_post' ) );
		add_filter( 'resuta_' . Resuta_Manager_Interested::POST_TYPE . '_save_value', array( &$this, 'filter_save_price' ), 10, 2 );
	}

	public function define_metaboxes()
	{
		add_meta_box(
			'resuta-metabox-interested-search-property',
			'Resultados dos Imóveis',
			array( 'Resuta_Manager_Interested_View', 'render_search_property' ),
			Resuta_Manager_Interested::POST_TYPE,
			'normal',
			'high'
		);

		add_meta_box(
			'resuta-metabox-interested-info-client',
			'Informações do Cliente',
			array( 'Resuta_Manager_Interested_View', 'render_info_client' ),
			Resuta_Manager_Interested::POST_TYPE,
			'normal',
			'high'
		);

		add_meta_box(
			'resuta-metabox-interested-info-property',
			'Informações do Imóvel',
			array( 'Resuta_Manager_Interested_View', 'render_info_property' ),
			Resuta_Manager_Interested::POST_TYPE,
			'normal',
			'high'
		);
	}

	public function filter_save_price( $value, $key )
	{
		if ( $key == Resuta_Manager_Interested::POST_META_PRICE_INITIAL )
			return Resuta_Manager_Utils_Helper::convert_float_for_sql( $value );

		if ( $key == Resuta_Manager_Interested::POST_META_PRICE_FINAL )
			return Resuta_Manager_Utils_Helper::convert_float_for_sql( $value );

		return $value;
	}

	public function nonce_valid_save_post( $is_valid )
	{
		$info_property_nonce = Resuta_Manager_Utils_Helper::post_method_params( self::NONCE_INFO_PROPERTY_NAME, false );
		$info_client_nonce   = Resuta_Manager_Utils_Helper::post_method_params( self::NONCE_INFO_CLIENT_NAME, false );

		if ( ! $info_property_nonce || ! wp_verify_nonce( $info_property_nonce, self::NONCE_INFO_PROPERTY_ACTION ) )
			return false;

		if ( ! $info_client_nonce || ! wp_verify_nonce( $info_client_nonce, self::NONCE_INFO_CLIENT_ACTION ) )
			return false;

		return true;
	}

	public function get_list( $args = array(), $is_parse_list = true )
	{
		$defaults = array(
			'post_type' => Resuta_Manager_Interested::POST_TYPE,
		);

		$args  = wp_parse_args( $args, $defaults );
		$query = Resuta_Manager_Utils_Helper::get_query( $args );

		if ( ! $is_parse_list )
			return $query;

		return $this->_parse_list( $query );
	}

	public function register_post_type()
	{
		register_post_type(
			Resuta_Manager_Interested::POST_TYPE,
			array(
				'labels' => array(
					'name'               => 'Clientes',
					'singular_name'      => 'Cliente Interessado',
					'all_items'          => 'Todos os clientes',
					'add_new'            => 'Adicionar novo',
					'add_new_item'       => 'Adicionar novo cliente',
					'edit_item'          => 'Editar cliente',
					'new_item'           => 'Novo cliente',
					'view_item'          => 'Visualizar clientes',
					'not_found_in_trash' => 'Nenhum cliente encontrado na lixeira',
				),
				'public'        	=> false,
				'show_ui'			=> true,
				'menu_position' 	=> 5,
				'supports'      	=> array( 'author' ),
				'taxonomies'        => array(
					Resuta_Manager_Taxonomy_Type_Controller::TAXONOMY,
					Resuta_Manager_Taxonomy_District_Controller::TAXONOMY,
					Resuta_Manager_Taxonomy_Transaction_Controller::TAXONOMY,
				),
				'menu_icon'			=> 'dashicons-id-alt',
				'capability_type'	=> Resuta_Manager_Interested::POST_TYPE,
			)
		);
	}

	/**
	 * Return an instance of this class.
	 *
	 * @since 1.0
	 * @return object A single instance of this class.
	 */
	public static function get_instance()
	{
		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public static function admin_url_create()
	{
		return admin_url( 'post-new.php?post_type=' . Resuta_Manager_Interested::POST_TYPE );
	}

	public static function admin_url_list()
	{
		return admin_url( 'edit.php?post_type=' . Resuta_Manager_Interested::POST_TYPE );
	}

	private function _parse_list( $wp_query )
	{
		if ( ! $wp_query->have_posts() )
			return false;

		$list = array();

		foreach ( $wp_query->posts as $diary )
			$list[] = new Resuta_Manager_Interested( $diary->ID );

		return $list;
	}
}
