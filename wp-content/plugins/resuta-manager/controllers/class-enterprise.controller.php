<?php
/**
 * Controller Enterprise
 *
 * @package Resuta Manager
 * @subpackage Enterprise
 * @since 1.0
 */
class Resuta_Manager_Enterprise_Controller
{
	/**
	 * Instance of this class.
	 *
	 * @since 1.0
	 * @var object
	 */
	protected static $instance = null;

	/**
	 * View
	 *
	 * @since 1.0
	 * @var object
	 */
	protected $view;

	/**
	 * Adds needed actions to create submenu and page
	 *
	 * @since 1.0
	 * @return void
	 */
	public function __construct()
	{
		add_action( 'edit_user_profile', array( &$this, 'proxy_page_profile' ) );
		add_action( 'show_user_profile', array( &$this, 'proxy_page_profile' ) );
		add_action( 'edit_user_profile_update', array( &$this, 'proxy_profile' ) );
		add_action( 'personal_options_update', array( &$this, 'proxy_profile' ) );
		add_action( 'edit_user_profile_enterprise', array( &$this, 'edit_page_profile' ) );
		add_action( 'save_user_profile_enterprise', array( &$this, 'save_page_profile' ) );
		add_action( 'after_setup_theme', array( &$this, 'define_image_sizes' ) );
		add_action( 'wp_dashboard_setup', array( &$this, 'widgets_remove_in_dashboard' ) );
		add_action( 'wp_dashboard_setup', array( &$this, 'widgets_add_in_dashboard' ) );
		add_action( 'resuta_create_new_user_enterprise', array( &$this, 'send_password_for_email' ), 10, 2 );
	}

	public function create_user( $args )
	{
		$name  = $args['name'];
		$pass  = $args['password'];
		$email = sanitize_email( $args['email'] );
		$type  = sanitize_title( $args['type'] );

		$user = array(
			'user_login' => sanitize_title( $name ) . '_' . time(),
			'user_email' => $email,
			'user_pass'  => $pass,
			'first_name' => $name,
			'role'		 => Resuta_Manager_Enterprise::ROLE,
		);

		$user_id = wp_insert_user( $user );

		$this->validation_new_user( $user_id );
		$this->define_meta_type_new_user( $user_id, $type );

		do_action( 'resuta_create_new_user_enterprise', $user_id, $pass );
	}

	public function send_password_for_email( $user_id, $pass )
	{
		$model = new Resuta_Manager_Enterprise( $user_id );

		$subject = 'Cadastro | Notificação de Senha';
		$headers = "from: Jornal de Imóvel <no-reply@jornaldeimovel.com.br>\ncontent-type: text/html; charset=UTF-8";
		$message = Resuta_Manager_Enterprise_View::render_email_password( $model, $pass );

		wp_mail( $model->email, $subject, $message, $headers );
	}

	public function validation_new_user( $user_id )
	{
		if ( is_wp_error( $user_id ) ) :
			echo json_encode(
				array(
					'mailSent' => false,
					'message'  => $user_id->get_error_message(),
				)
			);
			exit(0);
		endif;
	}

	public function define_meta_type_new_user( $user_id, $type )
	{
		if ( empty( $type ) )
			$type = Resuta_Manager_Enterprise::OPTION_TYPE_AGENT;

		update_user_meta( $user_id, Resuta_Manager_Enterprise::USER_META_TYPE, $type );
	}

	public function widgets_remove_in_dashboard()
	{
		if ( ! current_user_can( Resuta_Manager_Enterprise::ROLE ) )
			return;

		remove_meta_box( 'dashboard_activity', 'dashboard', 'normal' );
		remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
		remove_meta_box( 'dashboard_secondary', 'dashboard', 'side' );
	}

	public function widgets_add_in_dashboard() {

		if ( ! current_user_can( Resuta_Manager_Enterprise::ROLE ) )
			return;

		add_meta_box(
			'info_property_dashboard_widget',
			'Bem Vindo ao Jornal de Imóvel',
			array( 'Resuta_Manager_Enterprise_View', 'render_info_property_dashboard' ),
			'dashboard',
			'normal',
			'high'
		);
	}

	public function save_page_profile( $user_id )
	{
		$phones = Resuta_Manager_Utils_Helper::post_method_params( Resuta_Manager_Enterprise::USER_META_PHONES, false );
		$type   = Resuta_Manager_Utils_Helper::post_method_params( Resuta_Manager_Enterprise::USER_META_TYPE, false );
		$avatar = Resuta_Manager_Utils_Helper::post_method_params( Resuta_Manager_Enterprise::USER_META_AVATAR, false );

		if ( $avatar )
			update_user_meta( $user_id, Resuta_Manager_Enterprise::USER_META_AVATAR, $avatar );

		if ( $type )
			update_user_meta( $user_id, Resuta_Manager_Enterprise::USER_META_TYPE, $type );

		update_user_meta( $user_id, Resuta_Manager_Enterprise::USER_META_PHONES, $phones );
	}

	public function proxy_page_profile( $user )
	{
		if ( ( is_super_admin() || is_user_admin() ) || $user->has_cap( Resuta_Manager_Enterprise::ROLE ) )
			do_action( 'edit_user_profile_enterprise', $user );
	}

	public function edit_page_profile( $user )
	{
		Resuta_Manager_Enterprise_View::render_profile_enterprise( $user->ID );
	}

	public function proxy_profile( $user_id )
	{
		if ( ( is_super_admin() || is_user_admin() ) || user_can( $user_id, Resuta_Manager_Enterprise::ROLE ) )
			do_action( 'save_user_profile_enterprise', $user_id );
	}

	public function define_image_sizes()
	{
		//controller image
		$controller_image = Resuta_Manager_Image_Controller::get_instance();

		$controller_image->define(
			'attachment',
			array(
				Resuta_Manager_Enterprise::IMAGE_SIZE_AVATAR_SMALL => array( 222, 120, true ),
			)
		);
	}

	/**
	 * Return an instance of this class.
	 *
	 * @since 1.0
	 * @return object A single instance of this class.
	 */
	public static function get_instance()
	{
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Update user administrador caps Post Types
	 *
	 * @since 1.0
	 * @return void.
	 */
	public static function update_caps_administrator()
	{
		$role = get_role( 'administrator' );

		//post type property
		$capability_type = Resuta_Manager_Property::POST_TYPE;
		$role->add_cap( "edit_{$capability_type}" );
		$role->add_cap( "read_{$capability_type}" );
		$role->add_cap( "delete_{$capability_type}" );
		$role->add_cap( "edit_{$capability_type}s" );
		$role->add_cap( "edit_others_{$capability_type}s" );
		$role->add_cap( "publish_{$capability_type}s" );
		$role->add_cap( "read_private_{$capability_type}s" );
		$role->add_cap( "delete_{$capability_type}s" );
		$role->add_cap( "delete_private_{$capability_type}s" );
		$role->add_cap( "delete_published_{$capability_type}s" );
		$role->add_cap( "delete_others_{$capability_type}s" );
		$role->add_cap( "edit_private_{$capability_type}s" );
		$role->add_cap( "edit_published_{$capability_type}s" );

		//post type interested
		$capability_type = Resuta_Manager_Interested::POST_TYPE;
		$role->add_cap( "edit_{$capability_type}" );
		$role->add_cap( "read_{$capability_type}" );
		$role->add_cap( "delete_{$capability_type}" );
		$role->add_cap( "edit_{$capability_type}s" );
		$role->add_cap( "edit_others_{$capability_type}s" );
		$role->add_cap( "publish_{$capability_type}s" );
		$role->add_cap( "read_private_{$capability_type}s" );
		$role->add_cap( "delete_{$capability_type}s" );
		$role->add_cap( "delete_private_{$capability_type}s" );
		$role->add_cap( "delete_published_{$capability_type}s" );
		$role->add_cap( "delete_others_{$capability_type}s" );
		$role->add_cap( "edit_private_{$capability_type}s" );
		$role->add_cap( "edit_published_{$capability_type}s" );

		//taxonomy assign answers
		$role->add_cap( 'assign_terms_' . Resuta_Manager_Taxonomy_Type_Controller::TAXONOMY );
		$role->add_cap( 'assign_terms_' . Resuta_Manager_Taxonomy_Transaction_Controller::TAXONOMY );
		$role->add_cap( 'assign_terms_' . Resuta_Manager_Taxonomy_District_Controller::TAXONOMY );
	}

	public static function update_caps_enterprises()
	{
		$role = get_role( Resuta_Manager_Enterprise::ROLE );

		//post type property
		$capability_type = Resuta_Manager_Property::POST_TYPE;
		$role->add_cap( "edit_{$capability_type}" );
		$role->add_cap( "read_{$capability_type}" );
		$role->add_cap( "delete_{$capability_type}" );
		$role->add_cap( "edit_{$capability_type}s" );
		$role->add_cap( "publish_{$capability_type}s" );

		//post type interested
		$capability_type = Resuta_Manager_Interested::POST_TYPE;
		$role->add_cap( "edit_{$capability_type}" );
		$role->add_cap( "read_{$capability_type}" );
		$role->add_cap( "delete_{$capability_type}" );
		$role->add_cap( "edit_{$capability_type}s" );
		$role->add_cap( "publish_{$capability_type}s" );

		//taxonomy assign answers
		$role->add_cap( 'assign_terms_' . Resuta_Manager_Taxonomy_Type_Controller::TAXONOMY );
		$role->add_cap( 'assign_terms_' . Resuta_Manager_Taxonomy_Transaction_Controller::TAXONOMY );
		$role->add_cap( 'assign_terms_' . Resuta_Manager_Taxonomy_District_Controller::TAXONOMY );

		//outhers caps
		$role->add_cap( 'read' );
		$role->add_cap( 'upload_files' );
	}

	public static function create_role()
	{
		add_role( Resuta_Manager_Enterprise::ROLE, 'Empresas' );
		//set caps
		self::update_caps_administrator();
		self::update_caps_enterprises();
	}
}
