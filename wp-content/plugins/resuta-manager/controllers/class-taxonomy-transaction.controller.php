<?php
/**
 * Controller Taxonomy Transaction
 *
 * @package Resuta Manager
 * @subpackage Taxonomy Transaction
 * @since 1.0
 */
class Resuta_Manager_Taxonomy_Transaction_Controller
{
	/**
	 * Instance of this class.
	 *
	 * @since 1.0
	 * @var object
	 */
	protected static $instance = null;

	/**
	 * Return an instance of this class.
	 *
	 * @since 1.0
	 * @return object A single instance of this class.
	 */
	public static function get_instance()
	{
		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Tanoxonomy name
	 *
	 * @since 1.0
	 * @var string
	 */
	const TAXONOMY = 'resuta_tax_transaction';

	/**
	 * Options for transaction
	 *
	 * @since 1.0
	 * @var string
	 */
	const OPTION_RENT = 'aluguel';

	const OPTION_SALE = 'venda';

	/**
	 * Adds needed actions after plugin in enabled
	 *
	 * @since 1.0
	 * @return void
	 */
	public function __construct()
	{
		add_action( 'init', array( &$this, 'register_taxonomy' ), 0 );
	}

	/**
	 * Register all taxonomies of class
	 *
	 * @return void
	 */
	public function register_taxonomy()
	{
		register_taxonomy(
			self::TAXONOMY,
			NULL,
			array(
				'labels'        => array(
					'name'              => 'Tipo de Transação',
					'singular_name'     => 'Tipo de Transação',
					'menu_name'         => 'Tipo de Transação',
					'all_items'         => 'Todas as transações',
					'edit_item'         => 'Editar tipo de transação',
					'view_item'         => 'Visualizar tipo de transação',
					'update_item'       => 'Atualizar tipo de transação',
					'add_new_item'      => 'Adicionar novo tipo de transação',
					'new_item_name'     => 'Nome do novo tipo de transação',
					'parent_item'       => 'Tipo de transação pai',
					'parent_item_colon' => 'Tipo de transação pai:',
					'search_items'      => 'Pesquisar transações',
					'popular_items'     => 'Transações populares',
				),
				'public'            => true,
				'show_tagcloud'     => false,
				'show_admin_column' => true,
				'hierarchical'      => true,
				'rewrite'			=> array( 'slug' => 'tipo-de-transacao', 'with_front' => false ),
				'capabilities'      => array( 'assign_terms' => 'assign_terms_' . self::TAXONOMY ),
			)
		);
	}
}
