<?php
/**
 * Resuta Manager
 *
 * @package Resuta Manager
 * @version 1.0
 */
class Resuta_Manager
{
	protected $version = '1.0';

	protected static $instance = null;

	/**
	 * Slug Plugin
	 *
	 * @since 1.0
	 */
	const PLUGIN_SLUG = 'resuta-manager';

	/**
	 * Initialize the plugin by setting localization, filters, and administration functions.
	 *
	 * @since 1.0
	 */
	private function __construct()
	{
		// Load public-facing style sheet and JavaScript.
		add_action( 'admin_enqueue_scripts', array( &$this, 'scripts_admin' ) );
		add_action( 'admin_enqueue_scripts', array( &$this, 'styles_admin' ) );
		add_action( 'admin_enqueue_scripts_enterprise', array( &$this, 'styles_admin_enterprise' ) );

		Resuta_Manager_Image_Controller::get_instance();
		Resuta_Manager_Metas_Controller::get_instance();
		Resuta_Manager_Taxonomy_District_Controller::get_instance();
		Resuta_Manager_Taxonomy_Transaction_Controller::get_instance();
		Resuta_Manager_Taxonomy_Type_Controller::get_instance();
		Resuta_Manager_Featured_Controller::get_instance();
		Resuta_Manager_Property_Controller::get_instance();
		Resuta_Manager_Interested_Controller::get_instance();
		Resuta_Manager_Attributes_Controller::get_instance();
		Resuta_Manager_Address_Controller::get_instance();
		Resuta_Manager_Enterprise_Controller::get_instance();
		Resuta_Manager_Widget_Controller::get_instance();
	}

	/**
	 * Register Script Admin
	 *
	 * @since 1.0
	 * @return void.
	 */
	public function scripts_admin()
	{
		$this->_load_wp_media();

		wp_enqueue_script(
			self::PLUGIN_SLUG . '-admin-script',
			plugins_url( 'assets/javascripts/admin.script.min.js', __FILE__ ),
			array( 'jquery' ),
			filemtime( plugin_dir_path(  __FILE__  ) . 'assets/javascripts/admin.script.min.js' ),
			true
		);

		wp_localize_script(
			self::PLUGIN_SLUG . '-admin-script',
			'WPAdminVars',
			array(
				'ajaxUrl' => admin_url( 'admin-ajax.php' )
			)
		);
	}

	public function styles_admin()
	{
		wp_enqueue_style(
			self::PLUGIN_SLUG . '-admin-style',
			plugins_url( 'assets/css/admin.css', __FILE__ ),
			array(),
			filemtime( plugin_dir_path(  __FILE__  ) . 'assets/css/admin.css' )
		);

		//print css for user enterprises
		if ( current_user_can( Resuta_Manager_Enterprise::ROLE ) )
			do_action( 'admin_enqueue_scripts_enterprise' );
	}

	public function styles_admin_enterprise()
	{
		wp_enqueue_style(
			self::PLUGIN_SLUG . '-admin-enterprises-css',
			plugins_url( 'assets/css/admin-enterprises.css', __FILE__ ),
			null,
			filemtime( plugin_dir_path(  __FILE__  ) . 'assets/css/admin-enterprises.css' )
		);
	}

	/**
	 * Return an instance of this class.
	 *
	 * @since 1.0
	 * @return object A single instance of this class.
	 */
	public static function get_instance()
	{
		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public static function activate()
	{
		Resuta_Manager_Address_Controller::create_tables();
		Resuta_Manager_Enterprise_Controller::create_role();
	}

	public static function path_assets( $path )
	{
		return plugins_url( 'assets/' . $path, __FILE__  );
	}

	/**
	 * Loads wp.media class on pages that do not.
	 *
	 * @since 1.0
	 * @return void
	 */
	private function _load_wp_media()
	{
		global $pagenow;

		if ( did_action( 'wp_enqueue_media' ) )
			return;

		if ( in_array( $pagenow, array( 'user-edit.php', 'profile.php' ) ) )
			wp_enqueue_media();
	}
}
