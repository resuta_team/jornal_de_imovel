<?php
/*
Plugin Name: Gerenciador do site
Plugin URI: http://resuta.com.br/
Description: Site Jornal de Imóvel | Cadastro de imóveis
Version: 1.0
Author: Agência Resuta
Author URI: http://resuta.com.br/
License: GPL2
*/
class ClassAutoloader
{
	/**
	 * Instance of this class.
	 *
	 * @since 1.0
	 * @var object
	 */
	protected static $instance = null;

	/**
	 * Initialize the class auto loader
	 *
	 * @since 1.0
	 */
	public function __construct()
	{
		spl_autoload_register( array( $this, '_loader' ) );
	}

	/**
	 * Add custom loaders usage for outher plugins
	 *
	 * @since 1.0
	 * @param array $defaults
	 */
	public function add_custom_loaders( $defaults = array() )
	{
		return apply_filters( 'resuta_add_loaders', $defaults );
	}

	public function require_config( $item, $key, $pattern_lower )
	{
		$plugin_slug = $this->_replace_name( $item['base_class'] );

		if ( strpos( $pattern_lower, $plugin_slug ) === false )
			return;

		if ( $pattern_lower == $plugin_slug ) :
			require_once sprintf( '%s/class-%s.php', $item['base_file'], $pattern_lower );
			return;
		endif;

		$pattern_lower = str_replace( $plugin_slug . '-', '', $pattern_lower );
		$partial       = $this->_slip( $pattern_lower );

		if ( ! isset( $partial[1] ) ) :
			require_once sprintf( '%1$s/%3$ss/class-%2$s.php', $item['base_file'], $partial[0], 'model' );
			return;
		endif;

		require_once sprintf( '%1$s/%3$ss/class-%2$s.%3$s.php', $item['base_file'], $partial[0], $partial[1] );
	}

	/**
	 * Return an instance of this class.
	 *
	 * @since 1.0
	 * @return object A single instance of this class.
	 */
	public static function get_instance()
	{
		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	private function _loader( $class_name )
	{
		$pattern_lower = $this->_replace_name( $class_name );

		$loaders = $this->add_custom_loaders(
			array(
				array(
					'base_class' => 'Resuta_Manager',
					'base_file'  => dirname( __FILE__ ),
				),
			)
		);

		//$this->_dependencies( $class_name, $pattern_lower );

		array_walk( $loaders, array( $this, 'require_config' ), $pattern_lower );
	}

	private function _replace_name( $name )
	{
		return str_replace( '_', '-', strtolower( $name ) );
	}

	private function _slip( $class_name )
	{
		return preg_split( '/-(controller|view|helper|widget)/',  $class_name, 2, PREG_SPLIT_DELIM_CAPTURE );
	}

	private function _dependencies( $class_name, $pattern_lower )
	{
		if ( $class_name == 'Resuta_Manager_Utils_Helper' and ! class_exists( 'Resuta_Manager_Utils_Helper' ) ) :
			require_once sprintf( '%s/helpers/class-utils.helper.php', dirname( __FILE__ ) );
			return false;
		endif;
	}
}

ClassAutoloader::get_instance();

register_activation_hook( __FILE__, array( 'Resuta_Manager', 'activate' ) );

add_action( 'plugins_loaded', array( 'Resuta_Manager', 'get_instance' ) );

