Module('Resuta.ComponentSelectsAddress', function(ComponentSelectsAddress) {
	
	ComponentSelectsAddress.fn.initialize = function(container) {
		this.container = container;
		this.init();
	};

	ComponentSelectsAddress.fn.createComponents = function() {
		this.componentStates = Resuta.ChosenAddressRequest(
			  this.container.byData( 'attr-states' )
			, { placeholder_text : 'Selecione um estado...' }
		);
		
		this.componentCities = Resuta.ChosenAddressRequest(
			  this.container.byData( 'attr-cities' )
			, { placeholder_text : 'Selecione uma cidade...' }
		);

		//init components
		this.componentStates.init();
		//set disabled
		this.initComponentCities();
	};

	ComponentSelectsAddress.fn.initComponentCities = function() {
		if ( this.componentCities.isEmpty() ) {
			this.componentCities.disabled();
		}

		this.componentCities.init();
	};

	ComponentSelectsAddress.fn.init = function() {
		this.createComponents();
		this.addEventListener();
	};

	ComponentSelectsAddress.fn.addEventListener = function() {
		this.componentStates
			.event( 'request-done', this._statesDone.bind( this ) )
			.event( 'define-params', this._statesDefineParams.bind( this ) )
			.on( 'change', this._onChangeState.bind( this ) )
		;

		this.componentCities			
			.event( 'request-done', this._citiesDone.bind( this ) )
			.event( 'define-params', this._citiesDefineParams.bind( this ) )			
		;
	};

	ComponentSelectsAddress.fn._onChangeState = function() {		
		this.componentCities.clear();
		this.componentCities.changePlaceholder( 'Aguarde...' );		
		this.componentCities.emit( 'chosen:search-request', true );		
	};

	ComponentSelectsAddress.fn._statesDone = function(response) {		
		return response;
	};

	ComponentSelectsAddress.fn._citiesDone = function(response) {
		this.componentCities.enabled( true );		
		return response;
	};

	ComponentSelectsAddress.fn._statesDefineParams = function(data) {
		return {
			"action" : "address-request-states"
		};
	};

	ComponentSelectsAddress.fn._citiesDefineParams = function(data) {
		return {
			"action" : "address-request-cities",
			"state"  : this.componentStates.getValue()
		};
	};
});