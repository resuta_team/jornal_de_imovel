Module('Resuta.Application', function(Application) {

	Application.init = function(container) {
		container.find( '.input-tags-phone' ).tagsInput({
			placeholderColor : '#CCC',
			defaultText      : '(33) 0000-0000',
			width            : '98%',
		});

		Resuta.MaskAttribute();
	};

	Application['resuta_cpt_property'] = {
		action : function(container) {
			Resuta.FactoryComponent
				.create( container, 'SelectsAddress', '[data-component-address]' )
			;
		}
	};

	Application['resuta_cpt_interest'] = {
		action : function(container) {
			Resuta.FactoryComponent
				.create( container, 'SelectsAddress', '[data-component-address]' )
			;
		}
	};

	Application['user-edit'] = {
		before : function(container) {
			Resuta.ControlTabsUsers( container );
		},
		action : function(container) {
			Resuta.FactoryComponent
				.create( container, 'UploadFile', '[data-component-upload-file]' )
			;
		}
	};

	Application['profile'] = {
		before : function(container) {
			Application['user-edit'].before( container );
		},
		action : function(container) {
			Application['user-edit'].action( container );
		}
	};

}, {});
