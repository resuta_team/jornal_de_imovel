Module('Resuta.ChosenAddressRequest', function(ChosenAddressRequest) {

	ChosenAddressRequest.fn.initialize = function(element, optionsChosen) {
		this.element       = element;
		this.optionsChosen = ( optionsChosen || {} );
 	};

	ChosenAddressRequest.fn.init = function() {
		this.on   = jQuery.proxy( this.element, 'on' );
		this.emit = jQuery.proxy( this.element, 'trigger' );
		this.startArgsChosen();

		return this;
	};

	ChosenAddressRequest.fn.event = function(name, callback) {
		this[name] = ( this[name] || callback );

		return this;
	};

	ChosenAddressRequest.fn.startArgsChosen = function() {
		var ajaxDefault = {
			type 	 			: 'GET',
			url      			: this.getUrlAjax(),
			dataType 			: 'json',
			beforeSend  		: this._beforeSendSearchRequest.bind( this ),
			updatedDataRequest 	: this._doneUpdatedDataRequest.bind( this ),
			keepTypingMsg       : 'continue digitando...',
      		lookingForMsg       : 'Procurando',
		};

		this.element.ajaxChosen( ajaxDefault, this._doneSeachRequest.bind( this ), this.optionsChosen );
	};

	ChosenAddressRequest.fn.getUrlAjax = function() {
		return ( window.WPAdminVars || {} ).ajaxUrl;
	};

	ChosenAddressRequest.fn.getValue = function() {
		return this.element.val();
	};

	ChosenAddressRequest.fn.isEmpty = function() {
		return ( !jQuery.trim( this.element.val() ) );
	};

	ChosenAddressRequest.fn.disabled = function( isChosenUpated ) {
		this.element.attr( 'disabled', 'disabled' );

		if ( isChosenUpated ) {
			this.emit( 'chosen:updated' );
		}
	};

	ChosenAddressRequest.fn.clear = function() {
		this.element
			.find( 'option' )
			.remove()
		;

		this.emit( 'chosen:updated' );
	};

	ChosenAddressRequest.fn.enabled = function( isChosenUpated ) {
		this.element.removeAttr( 'disabled' );

		if ( isChosenUpated ) {
			this.emit( 'chosen:updated' );
		}
	};

	ChosenAddressRequest.fn.changePlaceholder = function(text) {
		this.element
			.next( '.chosen-container' )
			.find( '.chosen-single span' )
			.text( text )
		;	
	};

	ChosenAddressRequest.fn._doneSeachRequest = function(response) {
		return ( this['request-done'] || jQuery.noop ).call( null, response );
	};

	ChosenAddressRequest.fn._doneUpdatedDataRequest = function(data) {
		return ( this['define-params'] || jQuery.noop ).call( null, data );
	};

	ChosenAddressRequest.prototype._beforeSendSearchRequest = function() {
		( this['before-send'] || jQuery.noop ).call( null );
	};	
});