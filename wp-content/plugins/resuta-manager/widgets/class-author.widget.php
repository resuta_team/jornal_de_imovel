<?php
if ( ! function_exists( 'add_action' ) ) exit;

class Resuta_Manager_Author_Widget extends WP_Widget
{
	public function __construct()
	{
		parent::__construct(
			'widget_resuta_author',
			'Resuta | Autor do Imóvel',
			array(
				'description' => 'Widget de configuração dos dados do autor do imóvel.',
				'classname'   => 'widget-resuta-author',
			)
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance )
	{
		if ( ! is_singular( Resuta_Manager_Property::POST_TYPE ) )
			return;

		global $post;

		$model  = new Resuta_Manager_Enterprise( $post->post_author );
		$title  = $model->get_text_type();
		$avatar = $model->get_avatar_url_small();

		echo $args['before_widget'];

		if ( ! empty( $title ) )
			echo $args['before_title'] . $title . $args['after_title'];

		?>

		<?php if ( ! empty( $avatar ) ) : ?>
		<figure class="thumbnail">
			<a href="<?php echo esc_url( get_author_posts_url( $post->post_author ) ); ?>" title="<?php echo esc_attr( $title ); ?>">
				<img src="<?php echo esc_url( $avatar ); ?>" alt="imagem ou logo da empresa">
			</a>
		</figure>
		<?php endif; ?>

		<div class="info">
			<h4 class="title-perfil">
				<a href="<?php echo esc_url( get_author_posts_url( $post->post_author ) ); ?>"><?php echo esc_html( $model->display_name ); ?></a>
			</h4>
			<?php $this->_print_phone( $model ); ?>
			<?php $this->_print_email( $model ); ?>
		</div>
		<?php

		echo $args['after_widget'];
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance )
	{
		return $new_instance;
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance )
	{
		?>
		<p>
			Widget de configuração dos dados do autor do imóvel.
		</p>
		<?php
	}

	private function _print_phone( Resuta_Manager_Enterprise $model )
	{
		if ( ! (bool)$model->phones )
			return;

		$phones = explode( ',', $model->phones );

		?>
		<div class="phone">
			<span class="icon-title">Telefone(s)</span>
			<ul class="list">
				<li><?php echo implode( '</li><li>', $phones ); ?></li>
			</ul>
		</div>
		<?php
	}

	private function _print_email( Resuta_Manager_Enterprise $model )
	{
		?>
		<div class="email">
			<span class="icon-title">Email</span>
			<a href="mailto:<?php echo esc_attr( $model->email ); ?>" title="<?php echo esc_attr( $model->email ); ?>">
				<?php echo esc_attr( Resuta_Manager_Utils_Helper::limit_text( $model->email, 22 ) ); ?>
			</a>
		</div>
		<?php
	}
}
