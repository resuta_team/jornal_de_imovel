<?php
if ( ! function_exists( 'add_action' ) ) exit;

class Resuta_Manager_Message_Widget extends WP_Widget
{
	public function __construct()
	{
		parent::__construct(
			'widget_resuta_message',
			'Resuta | Formulário de Mensagens',
			array(
				'description' => 'Widget de configuração do formulário de mensagens, na single de imóveis.',
				'classname'   => 'widget-resuta-message',
			)
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance )
	{
		$title   = $this->_get_property( $instance, 'title' );
		$excerpt = $this->_get_property( $instance, 'excerpt' );

		if ( empty( $excerpt ) )
			return;

		echo $args['before_widget'];

		if ( ! empty( $title ) )
			echo $args['before_title'] . $title . $args['after_title'];

		echo apply_filters( 'widget_text', htmlspecialchars_decode( $excerpt, ENT_QUOTES ) );

		echo $args['after_widget'];
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance )
	{
		$instance['title']   = esc_html( $new_instance['title'] );
		$instance['excerpt'] = esc_textarea( $new_instance['excerpt'] );

		return $instance;
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance )
	{
		$title   = $this->_get_property( $instance, 'title' );
		$excerpt = $this->_get_property( $instance, 'excerpt' );

		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">Título do destaque</label>
			<input class="widefat"
				   id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
				   name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
				   type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'excerpt' ) ); ?>">Shortcode</label>
			<textarea class="widefat" placeholder='Cole o shortcode do formulário [contact-form-7 id="2"]'
				      id="<?php echo esc_attr( $this->get_field_id( 'excerpt' ) ); ?>"
				      name="<?php echo esc_attr( $this->get_field_name( 'excerpt' ) ); ?>"
				      cols="30" rows="4"><?php echo htmlspecialchars_decode( $excerpt, ENT_QUOTES ); ?></textarea>
		</p>
		<?php
	}

	private function _get_property( $instance, $property )
	{
		return ( isset( $instance[ $property ] ) ) ? $instance[ $property ] : '';
	}
}
