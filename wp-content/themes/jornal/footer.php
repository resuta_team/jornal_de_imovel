<?php if ( ! function_exists( 'add_action' ) ) exit; ?>
<?php
/**
 * The footer template file.
 *
 * @package WordPress
 * @subpackage Theme
 */
global $wp_theme;
?>

		<footer class="footer container">
			<div class="branding-footer">
				<a href="<?php echo esc_url( $wp_theme->site_url ); ?>" title="<?php echo esc_attr( $wp_theme->site_name ); ?>">
					<img src="<?php echo esc_url( $wp_theme->template_url ); ?>/assets/images/branding-footer.png" height="50" width="144" alt="marca da empresa">
				</a>
			</div>

			<?php
				wp_nav_menu(
					array(
						'theme_location'  => 'menu-social',
						'menu_class'      => 'social',
						'container'		  => false,
						'fallback_cd'	  => '',
					)
				);
			?>

			<div class="newsletter">
				<form action="">
					<label for="">Imóveis para você</label>
					<input type="text" placeholder="Digite seu e-mail">
					<input type="submit" value="ok">
				</form>
			</div>

			<?php get_template_part( 'template-parts/template-part', 'form-sign-up' ); ?>

		</footer>
	</div><!-- wrapper -->

	<?php wp_footer(); ?>
</body>
</html>
