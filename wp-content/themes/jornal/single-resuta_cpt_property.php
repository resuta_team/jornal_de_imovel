<?php if ( ! function_exists( 'add_action' ) ) exit; ?>
<?php
/**
 * The single template file.
 *
 * @package WordPress
 * @subpackage Theme
 */
get_header();
the_post();

if ( ! class_exists( 'Resuta_Manager_Property' ) )
	return;

$model = new Resuta_Manager_Property( get_the_ID() );
$type  = $model->get_name_type();
$price = $model->get_format_price( 'R$ ' );
?>
	<?php if ( ! empty( $type ) ) : ?>
	<section class="entry-title">
		<div class="container">
			<h1 class="title"><?php echo esc_html( $type ); ?> - CÓD: <?php echo esc_html( $model->code ); ?></h1>
		</div>
	</section>
	<?php endif; ?>

	<section class="container">
		<section class="content single-property">
			<?php if ( ! empty( $price ) ) : ?>
			<header class="header-price">
				<span class="price">Valor: <?php echo esc_html( $price ); ?></span>
			</header>
			<?php endif; ?>

			<div class="slider <?php echo ( ! $model->has_gallery() ) ? 'no-gallery' : '' ; ?>">
			<?php
				if ( class_exists( 'Apiki_WP_Gallery_Shortcode_Controller' ) )
					Apiki_WP_Gallery_Shortcode_Controller::the_gallery();
			?>
			</div>

			<div class="details">
				<h3 class="title large">Detalhes do Imóvel</h3>

				<ul class="list">
					<?php
						if ( class_exists( 'Resuta_Manager_Attributes_View' ) ) :
							Resuta_Manager_Attributes_View::render_qualifications_checked_front( $model->attributes );
							Resuta_Manager_Attributes_View::render_qualifications_number_front( $model->attributes );
						endif;
					?>
				</ul>
			</div>

			<div class="about">
				<h3 class="title large">Sobre o Imóvel</h3>
				<?php
					$content = preg_replace( '/\[gallery.+\]/', '', get_the_content() );
					echo apply_filters( 'the_content', $content );
				?>
			</div>

			<div class="maps">
				<h3 class="title large">Localização</h3>

				<div class="iframe-map"
				     data-component-map
				     data-attr-address="<?php echo esc_attr( $model->address->get_address_for_map() ); ?>">
					<span class="custom-spinner"></span>
				</div>
			</div>
		</section>

		<?php get_sidebar( 'property' ); ?>
	</section>

	<?php get_template_part( 'template-parts/template-part', 'similar-property' ); ?>

<?php get_footer(); ?>
