<?php

class WP_Theme_Routes
{
	public function module_js()
	{
		echo sprintf( 'data-route="%s"', $this->_is_route_by_page() );
	}

	private function _is_route_by_page()
	{
		global $pagename;

		if ( is_home() )
			return 'home';

		if ( class_exists( 'Resuta_Manager_Property' ) && is_singular( Resuta_Manager_Property::POST_TYPE ) )
			return 'single-property';

		if ( class_exists( 'Resuta_Manager_Property' ) && is_archive( Resuta_Manager_Property::POST_TYPE ) )
			return 'archive-property';

		return 'not-defined';
	}
}
