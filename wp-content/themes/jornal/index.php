<?php if ( ! function_exists( 'add_action' ) ) exit; ?>
<?php
/**
 * The index template file.
 * 
 * @package WordPress
 * @subpackage Theme
 */
global $wp_theme;

get_header();
?>
	<?php get_template_part( 'template-parts/template-part', 'home-featured' ); ?>

	<?php get_template_part( 'template-parts/template-part', 'main-filter' ); ?>

	<?php get_template_part( 'template-parts/template-part', 'listing-property' ); ?>

<?php get_footer(); ?>
