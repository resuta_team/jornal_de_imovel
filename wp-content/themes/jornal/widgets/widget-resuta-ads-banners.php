<?php
if ( ! function_exists( 'add_action' ) ) exit;

class Widget_B10_Ads_Banners extends WP_Widget
{
	public function __construct()
	{
		parent::__construct(
			'widget-b10-ads-banners',
			'B10 | Publicidade Banners',
			array(
				'description' => 'Widget que exibe um banner (imagem) fixo no tamanho de 300x100',
			),
			array(
				'width'  => 300,
				'height' => 350,
			)
		);
	}

	private function _get_property( $instance, $property )
	{
		return ( isset( $instance[ $property ] ) ) ? $instance[ $property ] : '';
	}

	public function widget( $args, $instance )
	{		
		$title      = $this->_get_property( $instance, 'title' );
		$link       = $this->_get_property( $instance, 'link' );
		$attachment = $this->_get_property( $instance, 'attachment' );
		$image      = wp_get_attachment_image( $attachment, 'widget-thumbnail-banners' );

		echo $args['before_widget'];

		?>
			<figure class="thumbnail">
				<a href="<?php echo esc_url( $link ); ?>" title="<?php echo esc_attr( $title ); ?>" target="_blank">
					<?php echo $image; ?>
				</a>
			</figure>
		<?php

		echo $args['after_widget'];
	}

	public function update( $new_instance, $old_instance )
	{
		$instance['title']      = esc_html( $new_instance['title'] );
		$instance['link']       = esc_url( $new_instance['link'] );
		$instance['attachment'] = intval( $new_instance['attachment'] );

		return $instance;
	}

	public function form( $instance )
	{
		$title      = $this->_get_property( $instance, 'title' );
		$link       = $this->_get_property( $instance, 'link' );
		$attachment = $this->_get_property( $instance, 'attachment' );
		$image_data = wp_get_attachment_image_src( $attachment, 'widget-thumbnail-banners' );
		$image_src  = ( isset( $image_data[0] ) ) ? $image_data[0] : '';
		
		?>		
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">Título</label>
				<input class="widefat"
					   id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
					   name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
					   type="text" value="<?php echo esc_attr( $title ); ?>" />
			</p>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'link' ) ); ?>">Link</label>
				<input class="widefat"
					   id="<?php echo esc_attr( $this->get_field_id( 'link' ) ); ?>"
					   name="<?php echo esc_attr( $this->get_field_name( 'link' ) ); ?>"
					   type="text" value="<?php echo esc_url( $link ); ?>" />
			</p>
			<div style="text-align: center;">
				<p>
					<input id="<?php echo esc_attr( $this->get_field_id( 'attachment' ) ); ?>"
						   name="<?php echo esc_attr( $this->get_field_name( 'attachment' ) ); ?>"
						   type="hidden" value="<?php echo esc_attr( $attachment ); ?>" />

					<img src="<?php echo esc_url( $image_src ); ?>" style="max-width: 300px; max-height: 100px; margin-top: 25px" id="<?php echo esc_attr( $this->get_field_id( 'image' ) ); ?>" />

					<p>
						<a href="javascript:;"
						   data-upload-file="<?php echo esc_attr( $this->get_field_id( 'attachment' ) ); ?>"
						   data-image-file="<?php echo esc_attr( $this->get_field_id( 'image' ) ); ?>"
						   class="custom-media-preview button" title="Adicionar imagem">Adicionar imagem</a>
						   <p>Imagem no tamanho 300x100</p>
					</p>
				</p>
			</div>
		<?php
	}
}

