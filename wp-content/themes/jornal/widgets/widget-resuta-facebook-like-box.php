<?php
if ( !function_exists( 'add_action' ) ) exit;

class Widget_B10_Facebook_Like_Box extends WP_Widget 
{
	private $_default_title = 'Facebook Like Box';
	private $_default_width = 300;

	public function __construct() 
	{
		parent::__construct(
			'widget-b10-facebook-like-box',
			'B10 | Facebook Like Box',
			array(
				'classname'   => 'facebook-like',
				'description' => 'Widget que exibe o box de likes do Facebook, para páginas.',
			)
		);
	}

	private function _get_property( $instance, $property, $default = '' )
	{
		return ( isset( $instance[ $property ] ) ) ? $instance[ $property ] : $default;
	}

	public function widget( $args, $instance ) 
	{
		$page_url = $this->_get_property( $instance, 'page_url' );
		$title 	  = $this->_get_property( $instance, 'title', $this->_default_title );
		$width    = $this->_get_property( $instance, 'width', $this->_default_width );

		if ( empty( $page_url ) )
			return;

		echo $args['before_widget'];

		echo $args['before_title'] . $title . $args['after_title'];
		
		?>
			<div class="fb-like-box" data-href="<?php echo esc_url( $page_url ); ?>" data-width="<?php echo intval( $width ); ?>" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>
		<?php

		echo $args['after_widget'];
	}

	public function update( $new_instance, $old_instance ) 
	{
		$instance['title']    = esc_html( $new_instance['title'] );
		$instance['page_url'] = esc_url( $new_instance['page_url'] );
		$instance['width']    = intval( $new_instance['width'] );

		return $instance;
	}

	public function form( $instance ) 
	{
		$page_url = $this->_get_property( $instance, 'page_url' );
		$title 	  = $this->_get_property( $instance, 'title', $this->_default_title );
		$width    = $this->_get_property( $instance, 'width', $this->_default_width );
		
		?>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">Título</label> 
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
			</p>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'page_url' ) ); ?>">Link da página do Facebook</label> 
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'page_url' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'page_url' ) ); ?>" type="text" value="<?php echo esc_attr( $page_url ); ?>" />
			</p>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'width' ) ); ?>">Largura</label> 
				<input class="small-text" id="<?php echo esc_attr( $this->get_field_id( 'width' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'width' ) ); ?>" type="text" value="<?php echo esc_attr( $width ); ?>" />
				<br/>
				<span class="description">Coloque um valor numerico de acordo com a largura da sua área de Widgets</span>
			</p>
		<?php
	}
}

