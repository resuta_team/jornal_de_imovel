<?php if ( ! function_exists( 'add_action' ) ) exit; ?>
<?php
/**
 * The archive template file.
 *
 * @package WordPress
 * @subpackage Theme
 */
global $wp_theme;

get_header();
?>
	<?php get_template_part( 'template-parts/template-part', 'main-filter' ); ?>

	<section class="list-cards container">

		<?php
			while( have_posts() ) :
				the_post();
				get_template_part( 'template-parts/template-part', 'card-property' );
			endwhile;
		?>

		<!-- <div class="more-card">
			<button type="button">Exibir mais imóveis</button>
		</div> -->
	</section><!-- list card -->

<?php get_footer(); ?>
