<?php if ( ! function_exists( 'add_action' ) ) exit; ?>

<aside class="sidebar">
	<!-- <div class="perfil">
		<h3 class="title-section large">Detalhes do Imóvel</h3>

		<figure class="thumbnail">
			<a href="" title=""><img src="http://placehold.it/222x120" alt=""></a>
		</figure>
		<div class="info">
			<h4 class="title-perfil"><a href="">Lorem ipsum dolor sit amet.</a></h4>
			<ul class="list">
				<li>Lorem ipsum dolor sit amet.</li>
				<li>Lorem ipsum dolor sit amet.</li>
				<li>Lorem ipsum dolor sit amet.</li>
			</ul>						
		</div>
	</div>

	<div class="form-more-info">
		<h3 class="title-section large">Detalhes do Imóvel</h3>
		<div class="wpcf7" id="wpcf7-f6-o1">
			<form action="/#wpcf7-f6-o1" method="post" class="wpcf7-form" novalidate="novalidate">
				<div style="display: none;">
					<input name="_wpcf7" value="6" type="hidden">
					<input name="_wpcf7_version" value="3.7.2" type="hidden">
					<input name="_wpcf7_locale" value="pt_BR" type="hidden">
					<input name="_wpcf7_unit_tag" value="wpcf7-f6-o1" type="hidden">
					<input name="_wpnonce" value="3c0ffaee02" type="hidden">
				</div>
				<p>
					<span class="wpcf7-form-control-wrap your-name">
					<input name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Nome" type="text">
					</span>
				</p>
				<p>
					<span class="wpcf7-form-control-wrap your-email">
					<input name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Email" type="email"></span>
				</p>
				<p>
					<span class="wpcf7-form-control-wrap your-message">
					<textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Testemunho"></textarea>
					</span>
				</p>
				<p><input value="Enviar" class="wpcf7-form-control wpcf7-submit" type="submit"><span style="visibility: hidden;" class="ajax-loader">&nbsp;</span></p>
				<div class="wpcf7-response-output wpcf7-display-none"></div>
			</form>
		</div>					
	</div> -->
	<?php dynamic_sidebar( 'sidebar-property' ); ?>
</aside>