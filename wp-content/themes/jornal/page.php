<?php if ( ! function_exists( 'add_action' ) ) exit; ?>
<?php
/**
 * The page template file.
 *
 * @package WordPress
 * @subpackage Theme
 */
global $wp_theme;

get_header();
the_post();
?>
	<section class="entry-title">
		<div class="container">
			<h1 class="title"><?php the_title(); ?></h1>
		</div>
	</section>

	<div class="container">
		<div class="content post hentry"><?php the_content(); ?></div>
	</div>

<?php get_footer(); ?>
