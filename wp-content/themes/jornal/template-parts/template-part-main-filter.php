<?php if ( ! function_exists( 'add_action' ) ) exit; ?>
<?php global $wp_theme; ?>

<section class="filter" id="section-filter">
	<div class="container">
		<form action="">
			<ul>
				<li class="field first">
					<label class="label" for="">Estado</label>
					<?php $wp_theme->address->the_states(); ?>
				</li>
				<li class="medium field" data-component-load-cities data-attr-state=".select-state">
					<label class="label" for="">Cidade</label>
					<?php $wp_theme->address->the_cities(); ?>
				</li>
				<li class="medium field">
					<label class="label" for="">Bairro</label>
					<?php $wp_theme->address->the_district(); ?>
				</li>
				<li class="medium field">
					<label class="label" for="">Tipo de Imóvel</label>
					<?php $wp_theme->address->the_type(); ?>
				</li>
				<li class="field radio">
					<span class="field-label">Transação</span>
					<?php $wp_theme->address->the_transaction(); ?>
				</li>
				<li class="field">
					<input type="submit" value="Buscar">
				</li>
			</ul>					
		</form>
	</div>
</section><!-- filter -->

<script id="template-select-options" type="text/x-handlebars-template">
	{{#each options}}
		<option value="{{value}}">{{text}}</option>
	{{/each}}
</script>