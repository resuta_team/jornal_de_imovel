<?php if ( ! function_exists( 'add_action' ) ) exit; ?>
<?php
	if ( ! class_exists( 'Resuta_Manager_Property_Controller' ) )
		return;

	$controller = Resuta_Manager_Property_Controller::get_instance();
	$query      = $controller->get_list( array(), false );
?>

<section class="list-cards container">

	<?php
		while( $query->have_posts() ) :
			$query->the_post();
			get_template_part( 'template-parts/template-part', 'card-property' );
		endwhile;

		wp_reset_postdata();
	?>

	<!-- <div class="more-card">
		<button type="button">Exibir mais imóveis</button>
	</div> -->
</section><!-- list card -->
