<?php if ( ! function_exists( 'add_action' ) ) exit; ?>
<?php
	if ( ! class_exists( 'Resuta_Manager_Featured_Controller' ) )
		return;

	$controller = Resuta_Manager_Featured_Controller::get_instance();
	$list       = $controller->get_list();

	if ( ! $list ) //is empty
		return;
?>

<section class="featured-slider">
	<ul class="slider container <?php echo ( count( $list ) > 1 ) ? '' : 'no-slider'; ?>">
		<?php foreach ( $list as $item ) : ?>
		<li class="item">
			<a href="<?php echo esc_url( $item->link ); ?>" title="<?php echo esc_attr( $item->title ); ?>">
				<img src="<?php echo esc_url( $item->thumbnail_url ); ?>" alt="imagem de destaque">
				<p class="caption"><?php echo esc_html( $item->excerpt ); ?></p>
			</a>
		</li>
		<?php endforeach; ?>
	</ul>
</section><!-- featured slider -->