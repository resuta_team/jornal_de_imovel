<?php if ( ! function_exists( 'add_action' ) ) exit; ?>

<div class="box-login" data-component-box-toggle>
	<?php if ( ! is_user_logged_in() ) : ?>
	<a class="btn-login" href="javascript:void(0);" data-action="open">Entrar</a>
	<?php else : ?>
	<a class="btn-login" href="<?php echo esc_url( admin_url( 'index.php' ) ); ?>">Entrar</a>
	<?php endif; ?>

	<div class="login" data-attr-form>
		<form action="<?php echo esc_url( site_url( 'wp-login.php', 'login_post' ) ) ?>" method="post">
			<ul>
				<li class="field">
					<label for="">Login:</label>
					<input type="text" placeholder="Digite seu email" name="log">
				</li>
				<li class="field">
					<label for="">Senha</label>
					<input type="password" name="pwd">
				</li>
				<li>
					<input class="btn" type="submit" value="Entrar">
				</li>
				<input type="hidden" name="redirect_to" value="<?php echo esc_url( admin_url( 'index.php' ) ); ?>" />
			</ul>

			<a class="forgot-password" title="Recuperar senha" href="<?php echo esc_url( wp_lostpassword_url() ); ?>">Esqueci minha senha</a>
		</form>
	</div>
</div>
