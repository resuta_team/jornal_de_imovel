<?php if ( ! function_exists( 'add_action' ) ) exit; ?>
<?php
	if ( ! class_exists( 'Resuta_Manager_Property_Controller' ) )
		return;

	$controller = Resuta_Manager_Property_Controller::get_instance();
	$query      = $controller->get_list_by_price( get_the_ID(), false );

	if ( ! $query->have_posts() )
		return;
?>

<section class="similar-properties">
	<div class="container">
		<h3 class="title large">Imóveis Similares</h3>
		<div class="list-cards">
		<?php
			while( $query->have_posts() ) :
				$query->the_post();
				get_template_part( 'template-parts/template-part', 'card-property' );
			endwhile;

			wp_reset_postdata();
		?>
		</div>
	</div>
</section>
