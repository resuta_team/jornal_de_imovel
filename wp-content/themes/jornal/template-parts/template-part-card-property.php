<?php if ( ! function_exists( 'add_action' ) ) exit; ?>
<?php
	if ( ! class_exists( 'Resuta_Manager_Property' ) )
		return;

	$model = new Resuta_Manager_Property( get_the_ID() );
?>

<div class="card">
	<?php if ( has_post_thumbnail() ) : ?>
	<figure class="thumbnail">
		<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
			<?php the_post_thumbnail( Resuta_Manager_Property::IMAGE_SIZE_MEDIUM ); ?>
			<figcaption class="code">código - <?php echo esc_html( $model->code ); ?></figcaption>
		</a>
	</figure>
	<?php endif; ?>

	<div class="info">
		<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
			<?php
				$fields = array(
					'type'     => $model->get_name_type(),
					'distrito' => $model->address->get_name_district(),
					'city'     => $model->address->get_city( ', ' ) . $model->address->get_state(),
					'price'    => $model->get_format_price( 'R$ ' ),
				);

				$fields = array_filter( $fields );

				foreach ( $fields as $class => $value ) :
					echo "<div class=\"{$class}\">{$value}</div>";
				endforeach;
			?>
		</a>

		<div class="more-info icon-property">
			<?php
				$fields = array(
					'room'      => $model->attributes->rooms,
					'dormitory' => $model->attributes->fourth,
					'toilet'    => $model->attributes->toilets,
				);

				$fields = array_filter( $fields );

				foreach ( $fields as $class => $value ) :
					echo "<span class=\"icon {$class}\">{$value}</span>";
				endforeach;
			?>
		</div>
	</div>
</div>

<?php
	//clear instance
	unset( $model );
?>
