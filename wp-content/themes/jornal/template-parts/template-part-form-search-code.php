<?php if ( ! function_exists( 'add_action' ) ) exit; ?>
<?php
global $wp_theme;
?>

<div class="quick-search">
	<form action="<?php echo esc_url( $wp_theme->get_url_search_code() ); ?>">
		<input id="search-code" name="code" type="text" placeholder="Código do Imóvel"
		       value="<?php echo esc_html( WP_Theme_Utils::get_method_params( 'code', false ) ); ?>">
		<input type="submit" value="ok">
	</form>
</div>
