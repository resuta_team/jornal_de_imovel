<?php if ( ! function_exists( 'add_action' ) ) exit; ?>
<?php
global $wp_theme;

$page_box = $wp_theme->get_page_by_slug( WP_Theme_Admin::PAGE_BOX_SLUG );

if ( ! $page_box )
	return;
?>

<div class="form-sign-up" data-component-box-toggle>
	<button data-action="open" class="btn-form" type="button">Cadastrar Imobiliária corretor</button>

	<div class="info" data-attr-form>
		<button data-action="close" class="btn-close">fechar</button>
		<div class="form">
			<?php echo apply_filters( 'the_content', $page_box->post_content ); ?>
		</div>
	</div>
</div>
