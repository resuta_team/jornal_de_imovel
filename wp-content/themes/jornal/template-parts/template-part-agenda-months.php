<?php if ( ! function_exists( 'add_action' ) ) exit; ?>
<?php 
	$current_month = date_i18n( 'm' );
	$months        = array( 'Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez' );
?>

<ul class="filter" data-component-month>
	<?php foreach ( $months as $key => $month ) : ?>
		<li class="item-event<?php echo ( ( $key + 1 ) == $current_month ) ? ' active' : '' ?>" data-month="<?php echo ( $key + 1 ); ?>">
			<a href="javascript:void(0);" title="<?php echo $month; ?>"><?php echo $month; ?></a>
		</li>
	<?php endforeach; ?>
</ul>