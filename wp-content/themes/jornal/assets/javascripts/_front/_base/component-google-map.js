Module('Resuta.ComponentGoogleMap', function(ComponentGoogleMap) {
	
	ComponentGoogleMap.fn.initialize = function(container) {
		this.container = container;
		this.address   = this.container.data( 'attr-address' );
		this.init();
	};

	ComponentGoogleMap.fn.init = function() {
		this.getScript();
	};

	ComponentGoogleMap.fn.getScript = function() {
		var url = 'https://maps.googleapis.com/maps/api/js?v3.exp&callback=Resuta.ComponentGoogleMap.onFinallyScript&key=AIzaSyDUX1SdMjfsjl30ycd9-yqmrMaa-FqPWgw';

		//create public function for callback
		ComponentGoogleMap.onFinallyScript = (function() {
			this._startGeocoder();
		}).bind(this);

		//request file
		jQuery.getScript( url );
	};

	ComponentGoogleMap.fn._startGeocoder = function() {
		var geo = new google.maps.Geocoder();

		if ( !this.address ) {
			return;
		}

		geo.geocode( { 'address' : this.address }, this._onDoneGeocoder.bind(this) );
	};

	ComponentGoogleMap.fn._onDoneGeocoder = function(results, status) {
		var result;

		if ( status == 'OK' ) {
			result = results[0].geometry.location;
			this.render( result.lat(), result.lng() );
		}
	};

	ComponentGoogleMap.fn.render = function(latitude, longitude) {
		var marker, map, options;
		
		options = {
	    	zoom					: 15,
	    	center					: new google.maps.LatLng( latitude, longitude ),
		    mapTypeId 				: google.maps.MapTypeId.ROADMAP,
		    mapTypeControlOptions	: {
		    	mapTypeIds: Array( google.maps.MapTypeId.ROADMAP, google.maps.MapTypeId.SATELLITE )
		    },
		};
	
		map    = new google.maps.Map( this.container.get(0), options );
		marker = new google.maps.Marker({
		    position : options.center,
		    map      : map
		});
	};
});  