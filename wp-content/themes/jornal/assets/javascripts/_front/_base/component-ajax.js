Module('Resuta.ComponentAjax', function(ComponentAjax) {
	ComponentAjax.fn.initialize = function() {
		this._createEmitter();
		this.addEventListener();
	};

	ComponentAjax.fn._createEmitter = function() {
		this.emitter = jQuery({});
		this.on      = jQuery.proxy( this.emitter, 'on' );
		this.fire    = jQuery.proxy( this.emitter, 'trigger' );
	};

	ComponentAjax.fn.addEventListener = function() {
		this.on( 'request-cities', this._onStartRequestCities.bind(this) );
	};

	ComponentAjax.fn.getUrlAjax = function() {
		return ( window.WPVars || {} ).urlAjax;
	};

	ComponentAjax.fn._onStartRequestCities = function(e, state) {
		this.ajax( 'request-cities', {
			'action' : 'address-request-cities-property',
			'state'  : state,
		});
	};

	ComponentAjax.fn.ajax = function(typeFire, data) {
		var ajax = jQuery.ajax({
			url       : this.getUrlAjax(),
			dataType  : 'json',
			data      : data
		});

		this.fire( 'before-' +  typeFire );
		ajax.done( jQuery.proxy( this, '_done', typeFire ) );
		ajax.fail( jQuery.proxy( this, '_fail', typeFire ) );
	};

	ComponentAjax.fn._done = function(typeFire, response) {
		this.fire( 'done-' + typeFire, [ response ] );
	};

	ComponentAjax.fn._fail = function(typeFire, throwError, status) {
		this.fire( 'fail-' + typeFire, [ throwError.responseJSON ] );
	};
});