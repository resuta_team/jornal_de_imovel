Module('Resuta.MenuRetract', function(MenuRetract) {
	MenuRetract.fn.initialize = function(container) {
		this.container   = container.find( '.header' );
		this.navigation  = container.find( '.navigation' );
		this.positionTop = this.navigation.offset().top;

		this.init();
	};

	MenuRetract.fn.init = function() {
		this.handleEvent();
	};

	MenuRetract.fn.handleEvent = function() {
		jQuery( window )
			.on( 'scroll', this.menuRetract.bind( this ) )
		;
	};

	MenuRetract.fn.menuRetract = function() {
		if ( window.pageYOffset >= this.positionTop ) {
			this.container.addClass( 'active' );
			return;
		}
		
		this.container.removeClass( 'active' );
	};
});