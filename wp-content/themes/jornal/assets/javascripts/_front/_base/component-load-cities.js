Module('Resuta.ComponentLoadCities', function(ComponentLoadCities) {

	ComponentLoadCities.fn.initialize = function(container) {
		this.container = container;
		this.states    = jQuery( this.container.data( 'attr-state' ) );
		this.cities    = this.container.find( '.select-city' );
		this.ajax      = Resuta.ComponentAjax();
		this.template  = null;
		this.init();
	};

	ComponentLoadCities.fn.init = function() {
		this.addEventListener();
		this.loadTemplate();
	};

	ComponentLoadCities.fn.loadTemplate = function() {
		this.template = jQuery( '#template-select-options' ).compileHandlebars();
	};

	ComponentLoadCities.fn.addEventListener = function() {
		this.states
			.on( 'change', this._onChangeState.bind(this) )
		;

		this.ajax
			.on( 'before-request-cities', this._onBeforeRequestCities.bind(this) )
			.on( 'done-request-cities', this._onDoneRequestCities.bind(this) )
		;
	};

	ComponentLoadCities.fn._onDoneRequestCities = function(e, response) {
		this.cities.html( this.template( response ) );
		this.cities.removeAttr( 'disabled' );
		this.cities.trigger( 'chosen:updated' );
		this.removeLoading();
	};

	ComponentLoadCities.fn._onBeforeRequestCities = function() {
		this.showLoading();
	};

	ComponentLoadCities.fn.showLoading = function() {
		this.cities
			.next( '.chosen-container' )
			.addClass( 'chosen-load' )
		;
	};

	ComponentLoadCities.fn.removeLoading = function() {
		this.cities
			.next( '.chosen-container' )
			.removeClass( 'chosen-load' )
		;
	};

	ComponentLoadCities.fn._onChangeState = function(event) {
		if ( this.states.val() ) {
			this.ajax.fire( 'request-cities', [ this.states.val() ] );
		}
	};
});