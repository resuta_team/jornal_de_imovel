Module('Resuta.FactoryComponent', function(FactoryComponent) {

	FactoryComponent.create = function(container, name, selector) {
		container.isExist(selector, jQuery.proxy( this, '_start', name ));
		return this;
	};

	FactoryComponent._start = function(name, elements) {
		//Component not defined return noop function
		if ( typeof Resuta['Component' + name] != 'function' ) {
			return jQuery.noop;
		}

		this._iterator(elements, Resuta['Component' + name]);
	};

	FactoryComponent._iterator = function(elements, constructor) {
		elements.each(function(index, element){
			constructor.call(null, jQuery(element));
		});
	};

}, {});