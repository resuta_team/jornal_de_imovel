Module('Resuta.Application', function(Application) {

	Application.init = function(container) {
		Resuta.FactoryComponent
			.create( container, 'BoxToggle', '[data-component-box-toggle]' )
		;

		container.find( '.radio-icheck, .wrap-radio input' ).iCheck({
			labelHover : false,
			cursor     : true
		});
	};

	Application.home = {
		action : function(container) {
			Resuta.FactoryComponent
				.create( container, 'LoadCities', '[data-component-load-cities]' )
			;

			container.find( '.featured-slider .slider:not(.no-slider)' ).bxSlider({
				controls: false
			});

			container.find( '.select-chosen' ).chosen();
		}
	};

	Application['archive-property'] = {
		action : function(container) {
			Resuta.FactoryComponent
				.create( container, 'LoadCities', '[data-component-load-cities]' )
			;

			container.find( '.select-chosen' ).chosen();
		}
	};

	Application['single-property'] = {
		action : function(container) {
			Resuta.FactoryComponent
				.create( container, 'GoogleMap', '[data-component-map]' )
			;

			container.find( '.similar-properties .list-cards' ).bxSlider({
				controls    : false,
				slideWidth  : 222,
				maxSlides   : 4,
				slideMargin : 30
			});
		}
	};

}, {});
