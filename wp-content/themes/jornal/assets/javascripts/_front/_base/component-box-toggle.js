Module('Resuta.ComponentBoxToggle', function(ComponentBoxToggle) {

	ComponentBoxToggle.fn.initialize = function(container) {
		this.container = container;
		this.form      = this.container.byData( 'attr-form' );
		this.init();
	};

	ComponentBoxToggle.fn.init = function() {
		this.addEventListener();
		this.transformFields();
	};

	ComponentBoxToggle.fn.transformFields = function() {
		this.container
			.find( '.field-password' )
				.attr( 'type', 'password' )
		;
	};

	ComponentBoxToggle.fn.addEventListener = function() {
		this.container
			.on( 'click', '[data-action=open]', this._onClickOpenForm.bind(this) )
			.on( 'click', '[data-action=close]', this._onClickCloseForm.bind(this) )
		;
	};

	ComponentBoxToggle.fn._onClickOpenForm = function() {
		this.form.fadeToggle( 200 );
	};

	ComponentBoxToggle.fn._onClickCloseForm = function() {
		this.form.fadeOut( 200 );
	};
});
