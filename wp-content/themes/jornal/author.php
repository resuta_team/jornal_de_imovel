<?php if ( ! function_exists( 'add_action' ) ) exit; ?>
<?php
/**
 * The author template file.
 *
 * @package WordPress
 * @subpackage Theme
 */
global $wp_theme;

get_header();

$author_id = get_query_var( 'author' );
$model     = new Resuta_Manager_Enterprise( intval( $author_id ) );
$avatar    = $model->get_avatar_url_small();
$phones    = $model->get_phones_espace();
?>

	<div class="wrap-author container">
		<?php if ( ! empty( $avatar ) ) : ?>
		<div class="thumbnail">
			<img src="<?php echo esc_url( $avatar ); ?>" alt="imagem ou logo da empresa">
		</div>
		<?php endif; ?>

		<div class="info">
			<div class="name"><?php echo esc_html( $model->display_name ); ?> - <?php echo esc_html( $model->get_text_type() ); ?></div>

			<?php if ( ! empty( $phones ) ) : ?>
			<div class="tel"><?php echo esc_html( $phones ); ?></div>
			<?php endif; ?>

			<?php echo apply_filters( 'the_content', $model->description ); ?>
		</div>
	</div>

	<section class="list-cards container">

		<?php
			while( have_posts() ) :
				the_post();
				get_template_part( 'template-parts/template-part', 'card-property' );
			endwhile;
		?>

		<!-- <div class="more-card">
			<button type="button">Exibir mais imóveis</button>
		</div> -->
	</section><!-- list card -->

<?php get_footer(); ?>
